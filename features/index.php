<?php
  $page_title = "Accessibility Features in KDE";
  $site_root = "../";
  include "accessibility.inc";
  include "header.inc";
?>

 <h2><a name="settings">Settings in System Settings</a></h2>
 <p>KDE has a number of mouse and keyboard related settings that can help
  handicapped people. You can configure them using System Settings.
 </p>
 <p>In KDE 4.x, Accessibility settings are under "General &gt; Personal &gt; Accessibility"
 </p>
 <p>More information:<br />
   <a href="http://docs.kde.org/stable/en/kdebase-workspace/kcontrol/kcmaccess/index.html">KDE 4.x manual: accessibility settings</a>
 </p>

<?php
  include_once ("footer.inc");
?>
