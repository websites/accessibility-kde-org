<?php
  $page_title = "Related Links";
  $site_root = "../";
  include "accessibility.inc";
  include "header.inc";
?>

<div id="quicklinks">[
 <a href="#accessibility">Accessibility Projects</a> |
 <a href="#kde">KDE Links</a> |
</div>
 
<h2><a name="accessibility">Accessibility Projects</a></h2>
 <br /><a href="http://www.speechinfo.org/fdawg/">Free Desktop Accessibility Working Group (FDAWG)</a>
 <br /><a href="http://trace.wisc.edu/">Trace R&amp;D Center</a>
 <br /><a href="http://www.w3.org/WAI/">Web Accessibility Initiative (WAI)</a>
 <br /><a href="http://validator.w3.org/">Web Accessibility Initiative (WAI)</a>
 <br /><a href="http://bobby.watchfire.com/bobby/html/en/index.jsp">Bobby, another html page accessibility validator</a>
 </p>

<h2><a name="kde">KDE Links</a></h2>
 <p><a href="http://projects.kde.org/projects/kde/kdeaccessibility/">KDE Accessibility source project</a>
 <br /><a href="http://techbase.kde.org/Projects/Usability">KDE Usability Project</a>
 <br /><a href="http://lists.kde.org/?l=kde-accessibility">kde-accessibility mailing list archive</a>
 <br /><a href="http://mail.kde.org/mailman/listinfo/kde-accessibility">kde-accessibility subscription info</a>
 </p>


<?php
  include_once ("footer.inc");
?>
