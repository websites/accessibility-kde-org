<?php
  $page_title = "About Us";
  $site_root = "../";
  include "accessibility.inc";
  include "header.inc";
?>

<div id="quicklinks">[
  <a href="#what">What KDE Accessibility Is About</a> |
  <a href="#matters">Why KDE Accessibility Matters</a> |
  <a href="#interviews">Interviews</a> ]
</div>

<h2 id="what">What KDE Accessibility Is About</h2>
<p>Our goal is to make the entirety of the K Desktop Environment and (by necessity)
  its underlying technologies usable by and as efficient as possible for disabled
  users of all types.
</p>
 
<p>We like to make a complete accessible desktop as a free alternative to the
  expensiveness of commercial assistive technologies. By cooperating with other
  free solutions, interoperability with other accessibility software programs
  (e.g. GNOME applications) can be ensured.
</p>
<p>Making all of KDE fully accessible is a huge task, but it also involves very
  small things. A missing keyboard shortcut might be just a bit annoying for
  most people, but it makes the programs unusable for others. This is why the KDE
  Accessibility Projects aims to raise the awareness of accessibility issues among all
  people involved in KDE.
</p>

<h2 id="matters">Why KDE Accessibility Matters</h2>
<p>KDE accessibility matters for a number of reasons.  There are legal
  and financial reasons: in many countries, including the United States and many
  nations in the EU, in order for the government to use technology (free or otherwise),
  it must be accessible to the disabled.  There are also what one might categorize
  as "moral" reasons: free software should not just be free for all, but also
  usable at a basic level by all.  And accessible to all.  For yet others making KDE
  accessible matters because of their love for KDE an desire to share
  "the KDE experience" with all or because it is a programming challenge.
</p>
<p>Perhaps the most important reason KDE accessibility matters is because
  using KDE and its applications is necessity: users of all physical abilities need
  to be able to access KDE applications, utilities, settings, and
  application-generated files in order to work and communicate with others.
</p>
<p>Finally, making the K Desktop Environment accessible helps Linux and other
  free desktop on which KDE runs become an even more ideal platform (in terms
  of cost and quality) and computing environment for disabled users (and all
  users).
</p>

<h2 id="interviews">Interviews</h2>
<ul>
<li><a href="gunnar.php">Interview with Gunnar Schmi Dt</a> (maintainer of the kdeaccessibility CVS module)</li>
<li><a href="ttsteam.php">Interview about Text-To-Speech Support in KDE</a></li>
</ul>

<?php
  include_once ("footer.inc");
?>
