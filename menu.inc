<?php
$this->setName ("KDE Accessibility Project");

$section =& $this->appendSection("Inform");
$section->appendLink("Home","");
$section->appendLink("KDE Home","http://www.kde.org/",false);
$section->appendDir("About Us","about");

$section =& $this->appendSection("Communicate");
$section->appendDir("Contact","contact");
$section->appendDir("Events","events");

$section =& $this->appendSection("Develop");
$section->appendDir("Developers' Information","developer");
$section->appendDir("Accessibility Reports","reports");

$section =& $this->appendSection("Explore");
$section->appendDir("Related Links","links");
$section->appendDir("Glossary","glossary");
?>
