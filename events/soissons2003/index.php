<?php
  $page_title = "Troph&eacute;es Du Libre 2003";
  $site_root = "../../";
  include "accessibility.inc";
  include "header.inc";
?>

<p>
 The<a href="images/trophy.png"><img align="right" style="margin-left:25px;margin-bottom:10px;" height="150" width="146" alt="Photo of the KDEAP trophy" src="images/trophy-small.png" /></a>
 KDE Accessibility Project is proud to have received a trophy at the international Free Software competition <a href="http://www.tropheesdulibre.org">Troph&eacute;es Du Libre</a> in Soissons, France.
</p>
<p>
 The following people contributed to this success: Gunnar Schmidt (KDEAP maintainer, KMouth), Olaf Schmidt (KDEAP maintainer, website, reports), Sarang Lakare and Michael Forster (KMag), Jeff Roush (KMousetool), Jos&eacute; Pablo Ezequiel Fern&aacute;ndez (kttsd) and JP Schnapper-Casteras (KDEAP founder).
 <br />
</p>

<?php
  $gallery = new ImageGallery("This is a gallery of photos of the international Free Software competition Trophees Du Libre");

  $gallery->addImage(
  "images/gunnarolaf-small.jpg",
  "images/gunnarolaf.jpg",
  301, 150,
  "Photo of Olaf and Gunnar on the stage",
  "The KDEAP trophy",
  "Olaf and Gunnar Schmidt showing the trophy for the KDEAP. In the background of the stage, the names of the winners are sprayed onto a wooden wall by two 'grapheurs'.");
  
  $gallery->addImage(
  "images/seated-small.jpg",
  "images/seated.jpg",
  263, 150,
  "Photo of all accessibility nominees sitting together",
  "The &quot;Accessibility Row&quot;",
  "During the day, we spent a lot of time together with the people from the GNOME Accessibility Project (GAP). The atmosphere between the accessibility projects was excellent. We used the opportunity to discuss possible interoperability effords.");
  
  $gallery->startNewRow();

  $gallery->addImage(
  "images/all-small.jpg",
  "images/all.jpg",
  220, 150,
  "Group photo on the stage",
  "All accessibility trophies",
  "Peter Korn (Gnopernicus), Simon Bates (GOK), the competition logo, Adriana Dascal (Gnopernicus), Olaf Schmidt (KDEAP), Gunnar Schmidt (KDEAP), showing the three trophies. Congratulations to Simon Bates for receiving the first trophy.");

  $gallery->addImage(
  "images/peter-small.jpg",
  "images/peter.jpg",
  207, 150,
  "Photo of Peter Korn explaining the Gnopernicus",
  "After the event",
  "Directly after the event, Peter Korn from the Sun Accessibility Team explained GOK and Gnopernicus to an interested visitor, after having shown the programs to the KDEAP and to Richard M. Stallman earlier that day.");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/rms-small.jpg",
  "images/rms.jpg",
  220, 150,
  "Photo of Richard M. Stallman",
  "RMS talking about Free Software",
  "During the morning of the event, Richard M. Stallman gave a French talk on Free Software, explaining it along the lines of 'Liberte, Egalite et Fraternite'. When the accessibility teams later received their trophies, Peter Korn explained how 'Liberte, Egalite et Fraternite' require accessibility as well.");

  $gallery->addImage(
  "images/gunnar-small.jpg",
  "images/gunnar.jpg",
  129, 150,
  "Photo of Gunnar Schmidt at breakfast",
  "Next morning",
  "After having received the trophy, the KDEAP maintainers were brought to Paris by a bus hired for all the nominees. They stayed in the Youth Hostel 'Cite de Sciences', and had two wonderful days thanks to a street map presented by one of the organizers.");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/eiffel-small.jpg",
  "images/eiffel.jpg",
  106, 150,
  "Photo of Olaf, the trophy and the Tour d'Eiffel",
  "The Tour d'Eiffel",
  "Olaf Schmidt in front of a famous tower in Paris, showing the trophy.");

  $gallery->addImage(
  "images/paris1.jpg",
  "images/paris1.jpg",
  221, 150,
  "Photo of some ships in a canal close to the Bastille",
  "Close to the Bastille",
  "A photo taken from the place of the Bastille");

  $gallery->startNewRow();

  $gallery->addImage(
  "images/paris2-small.jpg",
  "images/paris2.jpg",
  219, 150,
  "Photo of the Seine",
  "The Seine",
  "The Seine is extremely beautiful, not only at night.");

  $gallery->addImage(
  "images/paris3-small.jpg",
  "images/paris3.jpg",
  228, 150,
  "Photo of a bridge over the Seine",
  "On top of a Seine bridge",
  "The traffic on top of a Seine bridge.");

  $gallery->show();
?>

<?php
  include "footer.inc"
?>