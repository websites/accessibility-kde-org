<?php
  $page_title = "Participating Projects and Companies";
  $site_root = "../../";
  include "accessibility.inc";
  include "header.inc";
?>

<p>
  The <a href="./">Unix Accessibility Forum 2004</a> is open to everyone interested
  in the improvement of Unix Accessibility:
</p>

<ul>
  <li>Developers of Unix Software</li>
  <li>Manufacturers, developers and distributors of assistive techologies</li>
  <li>Disabled persons with interest in computers</li>
</ul>

<p>
  The following projects and companies are participating with talks and presentations:
</p>

<ul>
  <li><a href="http://www.baum.de/">BAUM Retec AG</a> / <a href="http://developer.gnome.org/projects/gap/">GNOME Accessibility Project</a></li>
  <li><a href="http://accessibility.freestandards.org/">FSG Accessibility Workgroup</a></li>
  <li><a href="http://www.ibm.com/">IBM</a></li>
  <li><a href="http://accessibility.kde.org/">KDE Accessibility Project</a> (organizer)</li>
  <li><a href="http://www.novell.com/">Novell</a> / <a href="http://www.suse.com">SuSE</a></li>
  <li><a href="http://see-by-touch.sourceforge.net/index_.html">See<sub>by</sub>Touch Project</a></li>
  <li><a href="http://www.trolltech.com/">Trolltech</a> (sponsor)</li>
</ul>

<?php
  include_once ("footer.inc");
?>

