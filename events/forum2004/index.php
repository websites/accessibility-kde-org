<?php
  $page_title = "Unix Accessibility Forum";
  $site_root = "../../";
  include "accessibility.inc";
  include "header.inc";
?>

<h2>22-23 August 2004 in Ludwigsburg</h2>
<p>
  The KDE Accessibility Project is proud to announce the first Unix Accessibility Forum.
  It will take place during the <a href="http://conference2004.kde.org/">KDE Community World
  Summit</a> in Ludwigsburg near Stuttgart. The conference language is English.
</p>

<p>
  The Unix Accessibility Forum is a meeting of developers and disabled people that wish to further
  the accessibility of Unix for people with disabilities.
</p>

<ul>
  <li><a href="announcement.php">Announcement</a></li>
  <li><a href="program.php">Overview and Schedule</a></li>
  <li><a href="participants.php">Participating Projects and Companies</a></li>
  <li><a href="registration.php">Registration</a></li>
</ul>

<p>
  Interoperability is especially important in order to ensure the accessibility
  of graphical user interfaces. This is why the KDE project is very interested
  in cooperating very closely with other projects and companies dealing with
  accessibility on Unix.
</p>

<p>
  The Unix Accessibility Forum is sponsored by <a href="http://www.trolltech.com">Trolltech</a>.
</p>

<?php
  include_once ("footer.inc");
?>

