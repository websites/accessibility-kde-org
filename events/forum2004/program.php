<?php
  $page_title = "Overview and Schedule";
  $site_root = "../../";
  include "accessibility.inc";
  include "header.inc";
?>

<table border="1">
<tr><th colspan="2"><a href="./">Unix Accessibility Forum 2004</a></th></tr>
<tr><th>Sunday, 22.8</th><th></th></tr>
<tr><th></th><th bgcolor="#FFE0FF">Development Talks<br/>(also part of the <a href="http://conference2004.kde.org/devconf.php">KDE Developers and Contributors Conference</a>)</th></tr>
<tr><td>13:00</td><td bgcolor="#FFE0FF">Opening Talk: Desktop Accessibility for Hackers<br /><em>Aaron M Leventhal (IBM)</em></td></tr>
<tr><td>14:00</td><td bgcolor="#FFE0FF">The State of Unix Accessibility<br /><em>Peter Korn (Sun Microsystems, GNOME Accessibility Project)</em></td></tr>
<tr><td>15:00</td><td>Coffee break</td></tr>
<tr><td>15:30</td><td bgcolor="#FFE0FF">Making Qt/KDE Applications Accessible<br /><em>Harald Fernengel (Trolltech, KDE Accessibility Project)</em></td></tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr><th>Monday, 23.8</th><th></th></tr>
<tr><th></th><th bgcolor="#FFFFCD">Development Talks</th></tr>
<tr><td>9:00</td><td bgcolor="#FFFFCD">Strategies To Improve Accessibility on Unix<br /><em>Janina Sajka (Chair of the FSG Accessibility Workgroup)</em></td></tr>
<tr><td>10:00</td><td bgcolor="#FFFFCD">Feeling Pictures with See<sub>by</sub>Touch - From Qt to KDE<br /><em>Klaus Rieger (See<sub>by</sub>Touch project)</em></td></tr>
<tr><th></th><th bgcolor="#CCF9FF">Presentations of Existing Solutions</th></tr>
<tr><td>11:00</td><td bgcolor="#CCF9FF">The Gnopernicus Architecture<br /><em>Thomas Friehoff and/or Draghi Puterity (BAUM Retec AG, GNOME Accessibility Project)</em></td></tr>
<tr><td>12:00</td><td>Lunch</td></tr>
<tr><td>13:00</td><td bgcolor="#CCF9FF">Blinux<br /><em>Marco Skambraks (Novell/SuSE)</em></td></tr>
<tr><td>14:00</td><td bgcolor="#CCF9FF">GOK and Dasher - technologies for people with physical disabilities<br /><em>Peter Korn (Sun Microsystems, GNOME Accessibility Project)</em></td></tr>
<tr><td>15:00</td><td>Coffee Break</td></tr>
<tr><td>15:30</td><td bgcolor="#CCF9FF">Accessibility with KDE - Presentation and Outlook<br /><em>Gunnar and Olaf Schmidt (KDE Acessibility Project)</em></td></tr>
<tr><td>16:30</td><td>End of Unix Accessibility Forum</td></tr>
</table>

<?php
  include_once ("footer.inc");
?>

