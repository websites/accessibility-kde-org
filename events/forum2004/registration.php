<?php
  $page_title = "Registration for the Unix Accessibility Forum 2004";
  $site_root = "../../";
  include "accessibility.inc";
  include "header.inc";
?>

<p>
  The <a href="./">Unix Accessibility Forum 2004</a> will take place during the KDE Community World Summit.
  If you wish to participate at both events, you need to register only once.
  KDE Contributors are therefore asked to register via the
  <a href="http://conference2004.kde.org/registration.php">KDE Community World Summit registration page</a>.
</p>

<form method="post" action="http://conference2004.kde.org/registration.php">


<h3>Register as Free Software Contributor or Private Participant</h3>
<p>
  Private participants, free software contributors and disabled people can register for free.
</p>
<input type="submit" name="register1private" alt="1" value="Register as Private Participant">
<p>
  Participants with no or little income that would otherwise not be able to attend the Unix
  Accessibility Forum should contact <a href="mailto:harry@kdevelop.org">Harald Fernengel</a>
  for sponsoring.
</p>


<h3>Register as Corporate Participant</h3>
<p>
  Corporate visitors are expected to pay a conference
  fee of 650.- EUR (or 500.- EUR for "early bird" registrations). However,
  we will not prevent you from registering as a free software contributor
  or private participant, if for some reason your company won't cover your
  registration fee. We just ask you to make the effort, and help us recover
  the costs of organizing this event, run by a free and non-profit Open
  Source Software project.
</p>
<input type="submit" name="register1company" alt="1" value="Register as Corporate Participant" />


<input type="hidden" name="devconference22" value="1" />
<input type="hidden" name="marathon23" value="1" />
</form>

<?php
  include_once ("footer.inc");
?>

