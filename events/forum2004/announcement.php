<?php
  $page_title = "Announcement";
  $site_root = "../../";
  include "accessibility.inc";
  include "header.inc";
?>

<p>
The KDE Accessibility Project is proud to announce the first
<a href="./">Unix Accessibility Forum</a> on 22-23 August 2004 in Ludwigsburg.
</p>

<p>
The Unix Accessibility Forum, sponsored by Trolltech, is a meeting of
developers and disabled people that wish to further the accessibility of
Unix for people with disabilities.
</p>

<p>
The forum will include developer discussions as well as talks and
presentations on existing approaches. A call for presentations will be
announced shortly. German Disability Associations have been invited to
join us for feedback on the presentations of existing assistive
technologies on Unix.
</p>

<p>
The Unix Accessibility Forum complements efforts like the FSG
Accessibility Workgroup and the Free Desktop Accessibility Working Group.
Members of Unix-related accessibility projects like the GNOME
Accessibility Project, the Mozilla Accessibility Project and the
OpenOffice.org Accessibility team as well as developers working on
X Accessibility, assistive device support, speech synthesis, etc. are
highly welcome.
</p>

<p>
We believe that accessibility is important for the general developer
community. This is why the Unix Accessibility Forum is scheduled to
coincide with the
<a href="http://conference2004.kde.org/">KDE Contributor World Conference</a>.
Participants taking part in both conferences need to register only once.
</p>

<p>
Participants with no or little income that would otherwise not be able to
attend the Unix Accessibility Forum should contact
<a href="mailto:harry@kdevelop.org">Harald Fernengel</a> for sponsoring.
</p>

<p>
Please also forward this invitation to other projects and developers who
might be interested.
</p>

<p>
Please contact the
<a href="mailto:kde-accessibility@kde.org">KDE Accessibility mailing list</a>
or <a href="mailto:ojschmidt@kde.org">Olaf Schmidt</a> if you have comments,
questions or suggestions.
</p>

<?php
  include_once ("footer.inc");
?>

