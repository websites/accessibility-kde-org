<?php
  $page_title = "KDE for Partially Sighted Users";
  $site_root = "../../";
  include "accessibility.inc";
  include "header.inc";
?>

<div class="wikitext">
  <br />
  <div class="titlebar">KDE for Partially Sighted Users - Gunnar Schmidt</div>
  <br />
  Slide showing a diagram of a cross section of an eyeball.  The human eye consists of several parts, the light first goes through the cornea which breaks the light, through the pupil and the lens, the lens also breaks the light but with a varying breaking factor, the broken light will then be converted into signals at the retina.  These signals are transported via the <a class="wiki external"  href="http://diffractive-optics.org/ "> optic</a><img border="0" class="externallink" src="img/icons/external_link.gif" alt="external link" /> nerve to the brain.<br />
  <br />
  Examples of eye-related problems.  The lens can have a wrong configuration so the eye is too far-sighted or too near-sighted.  This can be corrected with glasses.  Anisometropia means the two lenses have different focuses.  More difficult to correct with glasses is astigmatism means the lenses are not spherical so the eye has different focal lengths in different directions.  Cataracts means the cornea becomes opaque so the light can not pass through it.  Some problem at the retina include colour blindness or retinal detachment.  Amblyopia means the <a class="wiki external"  href="http://grin-optics.org">optic</a><img border="0" class="externallink" src="img/icons/external_link.gif" alt="external link" /> nerve does not or only poorly transports the signals to the brain.<br />
  <br />
  Resulting needs from this include larger screen contents, high contrast in case too little light passes into the eye, for colour blind people avoid certain colours.  Some people need to avoid too bright screen contents.  Generally good for people who have problems reading is speech output.<br />
  <br />
  Demos a high contrast theme.  Some colours were not updated, these colours are configured separately from central KDE configuration and there are some hard coded colours but not too many.  He switches to a yellow/blue colour scheme.<br />
  <br />
  The next part of important settings for partially sighted users are size related settings.  Predefined desktop themes, user defined text sizes and KDE has KMagnifier.  He shows changing font sizes, most get changed but some sizes are hard-coded.<br />
  <br />
  KDE has support for speech reading web pages, PDF documents and text files.  It can also read system notification messages.  For more go to the talk on Tuesday, Text to Speech Solutions.<br />
  <br />
  With screen magnifiers you can show only part of the screen at a time, have the magnification always in the same part of the screen, have a magnification that follows the mouse, the fourth is to magnify to the screen edge.  If you want to enlarge screen contents you can use several algorithms.  Either repeat each pixel several times which leads to large blocks for each pixel, you can blur the contents or you can try to vectorise the image and uses that to produce larger contents.  Unfortunately the third algorithm isn't supported by X so the current magnifiers don't support that magnification.<br />
  <br />
  Next important features for screen magnifiers are colour enhancement, currently linux magnifiers only support colour invertion.  You can replace one colour with another, you can remove colours and you can circulate the colour spectrum.<br />
  <br />
  Marking important screen positions, e.g. the mouse, the text cursor.  Options are drawing a square, circle, cross which goes to the edges of the screen.  Windows has large triangles above and below the cursor.<br />
  <br />
  KMagnifier is a simple magnifier in KDE.  Magnifies into a window or to a screen edge (new in KDE 3.5).  Demonstates KMag.  Sets it to magnify to the screen edge.  Inverts the colours, useful for people who can't read if there's too much white on the screen.  Lists the features of the Gnome magnifier.  Both KDE and Gnome magnifiers miss many features in Windows ones.  He is working on a new magnifier for KDE 4 based on x.org composite so you can use full screen magnification with only 1 screen.  He demos this new magnifier, parts of the screen under the mouse are enlarged while those away from the mouse are shrunk.  Shows it at a higher zoom, showing only part of the desktop at one time.  It already has some support for vectorising the pixels on the screen leading to smoother scaling of fonts etc.<br />
  <br />
  Qt 4's accessibility framework is based on AT-SPI, standard with GTK and Mozilla.  Java has Java Accessibility Framework, Openoffice has a bridge to this.  Features that need this include focus-tracking and cursor-tracking in a magnifier, screen reading, controlling applications remotely through assistive technologies.  KDE will support AT-SPI from version 4 onwards.<br />
  <br />

</div><!-- end wikitext div -->


<?php
  include_once ("footer.inc");
?>

