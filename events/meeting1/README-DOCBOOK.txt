The HTML files in this directory are created via meinproc
from the docbook file. After re-creating the pages, it is
important to remove every occurance of "help:/" before
putting the pages on-line - otherwise the layout won't
work on browsers other than Konqueror.