<?php
  $page_title = "Events";
  $site_root = "../";
  include "accessibility.inc";
  include "header.inc";
?>

<div id="quicklinks">[
 <a href="#akademy2008">Akademy 2008</a> |
 <a href="#akademy2007">Akademy 2007</a> |
 <a href="#akademy2006">Akademy 2006</a> |
 <a href="#akademy2005">Akademy 2005</a> |
 <a href="#akademy2004">KDE Community World Summit 2004 "aKademy"</a> |
 <a href="#soissons2003">Soissons 2003</a> |
 <a href="#first">First IRC Meeting</a> ]
</div>

<h2 id="akademy2008">Akademy 2008</h2>
<p>Olaf Schmidt presented again about Standardizing Accessibility at <a href="http://akademy2008.kde.org">Akademy 2008</a> in Sint-Katelijne-Waver, Belgium. 
<br />More information can be found here: <a href="http://akademy2008.kde.org/conference/presentation/44.php">Standardising accesibility - a game changer for Free Software?</a>.
</p>

<h2 id="akademy2007">Akademy 2007</h2>
<p>Olaf Schmidt presented about Accessibility issues in KDE 4.x applications at <a href="http://akademy2007.kde.org">Akademy 2007</a> in Glasgow, Scottland.
<br />More information can be found here: <a href="http://akademy2007.kde.org/conference/talks/24.php">Why accessibility bugs harm KDE and how to get rid of them</a>.
</p>

<h2 id="akademy2006">Akademy 2006</h2>
<p>Gunnar Schmi Dt gave a talk about Accessibility of KDE 4.x applications at <a href="http://akademy2006.kde.org">Akademy 2006</a> in Dublin, Ireland.
<br />More information can be found here: <a href="http://akademy2006.kde.org/conference/talks/40.php">Accessibility of KDE 4.x applications</a>.
</p>

<h2 id="akademy2005">Akademy 2005</h2>
<p>Three KDE Accessibility talks were given at the 
<a href="http://conference2005.kde.org/">KDE Users and Developer Conference</a> 26 August - 4 September, 2005 M&aacute;laga (Andalusia Region), Spain.
</p>

<p>Slides for the talk
<a href="http://conference2005.kde.org/cfp/develconf/gary.cramblitt.y.hynek.hanke-text-to-speech.solutions.for.kde.4.accesibility.php">TTS Solutions for KDE 4 Accessibility</a> given by
Gary Cramblitt and Hynek Hanke (Brailcom.org):
</p>

<ul>
<li>First half (
<a href="http://accessibility.kde.org/events/akademy2005/kttsd/html/ktts/ktts.html">KTTSD</a>).
</li>
<li>Second half (Speech-Dispatcher).
(<a href="http://accessibility.kde.org/events/akademy2005/speech-dispatcher/speechd.pdf">PDF</a>) (<a href="http://accessibility.kde.org/events/akademy2005/speech-dispatcher/speechd.tex">TEX source</a> requires HA-Prosper to compile)
</li>
</ul>

<p>Olaf Schmidt and Hynek Hanke gave the talk <u>Changes needed in KDE4 to increase accessibility</u>.</p>

<p>A <a href="http://accessibility.kde.org/events/akademy2005/KDEforPartiallySighted.php">
transcript</a> of the talk by Gunnar Schmi Dt, 
<a href="http://conference2005.kde.org/cfp/userconf/gunnar.schmidt-partially.sighted.php">
KDE for partially sighted users</a>, is available.
</p>

<p>Audio and video downloads of all three talks are available at
<a href="http://ktown.kde.org/akademy2005/unprocessed/">
ktown.kde.org/akademy2005/unprocessed/</a>.  Note: Skip the first 20 minutes of
the TTS Solutions talk.  Technical difficulties.</p>

<h2 id="akademy2004">aKademy 2004</h2>
<p>A few presentations about accessibility took place in Ludwigsburg (Stuttgart Region), Germany during the "Unix Accessibility Forum" by employees of IBM, Sun, and Trolltech.<br />
More information about these presentations can be found here: <a href="http://conference2004.kde.org/sched-devconf.php">Unix Accessibility Forum</a></p>

<h2 id="soissons2003">International Free Software Competition 2003 in Soissons, France</h2>
<p>
 The KDE Accessibility Project is proud to have received a trophy at the international Free Software competition <a href="http://www.tropheesdulibre.org">Troph&eacute;es Du Libre</a> in Soissons, France.
</p>
<p>
 Further information:
 <br /><a href="soissons2003/">Photos of the event in Soissons</a>
</p>

<h2 id="first">First IRC Meeting</h2>
<p>
 An extremely busy IRC meeting took place on Friday, Sept 22, 2002, at 22 UTC.
 It was a great pleasure to chat with the other people involved with the KDE
 Accessibility Project, and it was very encouraging to see the various subprojects
 of KDEAP developing.
</p>
<p>
 Further information:
 <br /><a href="meeting1/">Detailed summary of meeting 1</a>
</p>


<?php
  include_once ("footer.inc");
?>

