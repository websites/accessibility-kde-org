<?php
  $page_title = "KDE.org Web Site Accessibility Report";
  $site_root = "../";
  include "accessibility.inc";
  include "header.inc";
?>

<div id="quicklinks">[
 <a href="#criteria">Criteria for Page Structure Evaluation</a> |
 <a href="#kderelated">Evaluation of KDE-Related Websites</a> |
 <a href="#cms">Evaluation of CMS/Wiki/Blog Websites</a> ]
</div>

<h2 id="criteria">Criteria for Page Structure Evaluation</h2>

<p>
  This is a list of criteria for evaluating the page structure of
  KDE-related websites as discussed on the kde-www-devel mailing list.
  Both the general critera and the accessibility criteria are ordered
  by priority: The most important are on top. The rational for the sorting
  is that it is not enough for a website to be standard-conformant but to
  fail in real-world browsers.
</p>

<ol type="A">
  <li>General criteria
    <ol>
      <li><a name="a1"/>Works with Konqueror 3.x and preferably even Konqueror 2.x
        <br/>Rational: It will be used for the
        <a href="http://www.konqueror.org/">Konqueror Website</a> as well.
      </li>
      <li><a name="a2"/>Works with the most common versions of Internet Explorer and Firefox
        <br/>Rational: Standards do not replace real-world tests.
      </li>
      <li><a name="a3"/>Works without Javascript
      </li>
      <li><a name="a4"/>Meets the XHTML standard
      </li>
      <li><a name="a5"/>Design (CSS), page structure (XHTML) and page content are cleanly separated
      </li>
    </ol>
  </li>

  <li>Accessibility criteria
    <ol>
      <li><a name="b1"/>Text browsers: The page structure is still usable when CSS and tables are ignored
        (long link lists only after the page content, hidden navigation links for text browsers, bonus points for no tables)
        <br/>Rational: This is important for blind users and for some mobile devices.
        <br/>Implementation hints: This is best tested with lynx. Navigation links for text
        browsers (e.g "Skip to content") can be hidden with style="display:none".
      </li>
      <li><a name="b2"/>Works with very large font sizes, and has enough contrast
        <br/>Rational: This is important for partially sighted users.
        <br/>Implementation hints: Do not use hardcoded font sizes or placement.
        <a href="../oxygen.php">Evaluate the contrast of colors</a> and make sure
        to specify all colors (background, foreground, links).
      </li>
      <li><a name="b3"/>Provides consistent access keys with the other KDE sites
        <br/>Rational: This is important for many users, e.g. with mobility or vision impairments.
        <br/>Implementation hints: the accesskeys are described on the page
        <a href="http://www.kde.org/media/accesskeys.php">Access Keys within KDE.org</a>.
        They will be reworked for the kde.org redesign, though.
      </li>
      <li><a name="b4"/>Provides a high contrast style sheet
        <br/>Rational: This is important for partially sighted users.
        <br/>Implementation hints: See www.kde.org or this site for an example.
      </li>
      <li><a name="b5"/>Has accessible markup for images and for tables
        <br/>Rational: This is important blind users.
        <br/>Implementation hints: Provide an "alt" tag for images, and a "summary" tag for tables.
      </li>
      <li><a name="b6"/>Meets the Web Content Accessibility Guidelines
        <br/>Rational: Open standards are very important, but do not replace real-world tests.
        <br/>Implementation hints: A useful checking tool is <a href="http://www.sidar.org/hera/">HERA</a>.
      </li>
    </ol>
  </li>
</ol>

<h2 id="kderelated">Evaluation of KDE-Related Websites</h2>
<p>
  This is a quick check evaluating the page structure used.
  There can be aditional accessibility problems on individual pages.
  Note that in many cases, it is very easy to improve an "almost" to a "yes",
  or a "no" to an "A", or an "A" to a "AA".
</p>

<p>
Assessment date: August 3, 2006<br/>
Update: August 5, 2006 www-devel.kde.org has been fixed<br/>
Update: August 8, 2006 conference2006.kde.org has been fixed<br/>
</p>

<table summary="How accessible are the various KDE websites?"><tr>
<th>Website</th>
<th><a href="#b1">Text browsers</a></th>
<th><a href="#b2">Large font sizes</a></th>
<th><a href="#b3">Access keys</a></th>
<th><a href="#b4">High Contrast CSS</a></th>
<th><a href="#b5">Image and table tags</a></th>
<th><a href="#b6">WCAG<br/>(A, AA or AAA)</a></th>
</tr><tr>
<td>www.kde.org<br/>accessibility.kde.org<br/>developer.kde.org<br/>konqueror.org<br/>pim.kde.org etc.<br/>(Capacity with old layout)</td>
<td class="green">yes (tables)</td><td class="lime">almost</td><td class="green">yes</td><td class="green">yes</td><td class="green">yes</td><td class="lime">A</td>
</tr><tr>
<td>conference2006<br/>www-devel.kde.org<br/>(Capacity with new layout)</td>
<td class="green">yes</td><td class="green">yes</td><td class="green">yes</td><td class="green">yes</td><td class="green">yes</td><td class="green">AAA</td>
</tr><tr>
<td>appeal.kde.org</td>
<td class="lime">almost</td><td class="lime">almost</td><td class="orange">(no)</td><td class="red">no</td><td class="green">yes</td><td class="lime">A</td>
</tr><tr>
<td>bugs.kde.org</td>
<td class="green">yes (tables)</td><td class="lime">almost</td><td class="green">yes</td><td class="green">yes</td><td class="green">yes</td><td class="lime">A</td>
</tr><tr>
<td>docs.kde.org</td>
<td class="green">yes (tables)</td><td class="lime">almost</td><td class="green">yes</td><td class="green">yes</td><td class="green">yes</td><td class="lime">A</td>
</tr><tr>
<td>dot.kde.org</td>
<td class="orange">(no)</td><td class="green">yes</td><td class="red">no</td><td class="red">no</td><td class="green">yes</td><td class="lime">A</td>
</tr><tr>
<td>englishbreakfast<br/>network.org</td>
<td class="lime">almost</td><td class="green">yes</td><td class="red">no</td><td class="red">no</td><td class="green">yes</td><td class="lime">A</td>
</tr><tr>
<td>kate-editor.org</td>
<td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="green">yes</td><td class="lime">A</td>
</tr><tr>
<td>kdedevelopers.org</td>
<td class="lime">almost</td><td class="green">yes</td><td class="red">no</td><td class="red">no</td><td class="green">yes</td><td class="lime">A</td>
</tr><tr>
<td>forum.kde.org</td>
<td class="red">no</td><td class="green">yes</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td>
</tr><tr>
<td>kde-look.org etc.</td>
<td class="red">no</td><td class="green">yes</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td>
</tr><tr>
<td>lists.kde.org</td>
<td class="lime">almost</td><td class="green">yes</td><td class="red">no</td><td class="red">no</td><td class="green">yes</td><td class="lime">A</td>
</tr><tr>
<td>lxr.kde.org</td>
<td class="lime">almost</td><td class="lime">almost</td><td class="red">no</td><td class="red">no</td><td class="green">yes</td><td class="lime">A</td>
</tr><tr>
<td>oxygen-icons.org</td>
<td class="lime">almost</td><td class="lime">almost</td><td class="red">no</td><td class="red">no</td><td class="green">yes</td><td class="lime">A</td>
</tr><tr>
<td>planetkde.org</td>
<td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td>
</tr><tr>
<td>plasma.kde.org</td>
<td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td>
</tr><tr>
<td>phonon.kde.org</td>
<td class="orange">(no)</td><td class="green">yes</td><td class="red">no</td><td class="red">no</td><td class="green">yes</td><td class="lime">A</td>
</tr><tr>
<td>radio.kde.org</td>
<td class="green">yes</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td>
</tr><tr>
<td>solid.kde.org</td>
<td class="red">no</td><td class="green">yes</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td>
</tr><tr>
<td>spread.kde.org</td>
<td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="green">yes</td><td class="lime">A</td>
</tr><tr>
<td>websvn.kde.org</td>
<td class="green">yes (tables)</td><td class="lime">almost</td><td class="green">yes</td><td class="red">no</td><td class="green">yes</td><td class="lime">A</td>
</tr><tr>
<td>wiki.kde.org</td>
<td class="lime">almost</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="green">yes</td><td class="red">no</td>
</tr></table>


<h2 id="cms">Evaluation of CMS/Wiki/Blog Websites</h2>
<p>
  This is a quick check evaluating of the page structure used.
  There can be additional accessibility problems on individual pages.
  No evaluation has been made how easy it is in the CMS to fix the problems.
  This is merely meant as an indicator how well the CMS authors are familiar
  with accessibility needs.
</p>

<p>
Assessment date: August 5, 2006
</p>

<table summary="How accessible are the various KDE websites?"><tr>
<th>Website</th>
<th><a href="#b1">Text browsers</a></th>
<th><a href="#b2">Large font sizes</a></th>
<th><a href="#b3">Access keys</a></th>
<th><a href="#b4">High Contrast CSS</a></th>
<th><a href="#b5">Image and table tags</a></th>
<th><a href="#b6">WCAG<br/>(A, AA or AAA)</a></th>
</tr><tr>
<td>awf-cms.org</td>
<td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="green">yes</td><td class="red">no</td>
</tr><tr>
<td>drupal.org</td>
<td class="orange">(no)</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="green">yes</td><td class="lime">A</td>
</tr><tr>
<td>joomla.org</td>
<td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="green">yes</td><td class="red">no</td>
</tr><tr>
<td>mamboserver.org</td>
<td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td>
</tr><tr>
<td>mediawiki.org</td>
<td class="green">yes</td><td class="green">yes</td><td class="green">yes</td><td class="red">no</td><td class="green">yes</td><td class="lime">A</td>
</tr><tr>
<td>mmbase.org</td>
<td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="green">yes</td><td class="lime">A</td>
</tr><tr>
<td>plone.org</td>
<td class="lime">almost</td><td class="red">no</td><td class="green">yes</td><td class="orange">no (but larger text CSS)</td><td class="green">yes</td><td class="lime">A</td>
</tr><tr>
<td>typo3.org</td>
<td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="green">yes</td><td class="red">no</td>
</tr><tr>
<td>wordpress.org</td>
<td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td><td class="red">no</td>
</tr></table>

<?php
  include_once ("footer.inc");
?>
