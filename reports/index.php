<?php
  $page_title = "Accessibility Reports";
  $site_root = "../";
  include "accessibility.inc";
  include "header.inc";
?>

<p>The first step in improving accessibility is always naming the existing
 issues. The Accessibility Reports published here will help developers to
 see where KDE needs to be improved.
</p>

<div id="quicklinks">[
 <a href="#websites">KDE.org Web Site Accessibility Report</a> |
 <a href="#vpat">KDE Voluntary Product Accessibility Template (VPAT)</a> |
 <a href="#koffice">KOffice Accessibility Assessment</a> ]
</div>

<h2 id="websites">KDE.org Web Site Accessibility Report</h2>
 <p>This report deals with issues of the kde.org web site familiy that are already being addressed
  through the new design.
  <br /><a href="websites.php">KDE.org Web Site Accessibility Report</a>
 </p>

<h2 id="vpat">KDE Voluntary Product Accessibility Template (VPAT)</h2>
<p>Voluntary Product Accessibility Templates (VPAT) are a way for software vendors to assist U.S. Federal agencies when acquiring software, by informing the agency how well the software meets the requirements of Section 508 of the U.S. Rehabilitation Act.
</p>

<p><b>KDE Version 3.4.1 VPAT:</b>
<br /><a href="vpat_kde3.4.1.sxw">OpenOffice.org/StarOffice v1 (.sxw) 12kB</a>
<br /><a href="vpat_kde3.4.1.pdf">PDF 249kB</a>
</p>
<p>The VPATs for KDE version 3.4.2 and 3.5 would be similar to above.
</p>

<h2 id="koffice">KOffice Accessibility Assessment</h2>
<p>Also contains information concerning upcoming v1.5 release.</p>
<p><b>KOffice v1.4.1 Accessibility Assessment:</b>
<br /><a href="koffice-1.4.1-accessibility-assessment.sxw">OpenOffice.org/StarOffice v1 (.sxw) 27kB</a>
<br /><a href="koffice-1.4.1-accessibility-assessment.odt">Open Document Format (.odt) 30kB</a>
<br /><a href="koffice-1.4.1-accessibility-assessment.pdf">PDF 190kB</a>
</p>

<h3>Disclaimer</h3>
<p>These documents are for informational purposes only.  KDE e.V., Trolltech, and the preparer make no warranties, express, implied, or statutory, in these documents.  The information in these document represents the current view of the preparer on the issues discussed as of the date of publication.  It should not be interpreted to be a commitment on the part of KDE e.V., Trolltech, or the preparer.
</p>

<?php
  include_once ("footer.inc");
?>
