<?php
    global $plasmaMenu;
    
    $plasmaMenu->addMenu("Inform");
        $plasmaMenu->addMenuEntry("Home","/");
        $plasmaMenu->addMenuEntry("KDE Home","http://www.kde.org/");
        $plasmaMenu->addMenuEntry("About Us","/about");
    
    $plasmaMenu->addMenu("Communicate");
        $plasmaMenu->addMenuEntry("Contact","/contact");
        $plasmaMenu->addMenuEntry("Events","/events");
    
    $plasmaMenu->addMenu("Develop");
        $plasmaMenu->addMenuEntry("Developers' Information","/developer");
        $plasmaMenu->addMenuEntry("Accessibility Reports","/reports");
    
    $plasmaMenu->addMenu("Explore");
        $plasmaMenu->addMenuEntry("Related Links","/links");
        $plasmaMenu->addMenuEntry("Glossary","/glossary");
?>
