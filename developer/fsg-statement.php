<?php
  $page_title = "KDE Statement on the Creation of the Free Standards Accessibility Workgroup";
  $site_root = "../";
  include "accessibility.inc";
  include "header.inc";
?>

<div id="quicklinks"> [
 <a href="#statement">Statement</a> |
 <a href="#kdeap">About the KDE Accessibility Project</a> |
 <a href="#fsg">About the FSG Accessibility Workgroup</a> ]
</div>

<h2 id="statement">Statement</h2>
<p>
 <q>Although there are many ongoing efforts to help persons with disabilities use
 Linux systems, most Linux GUIs are still inaccessible to blind persons and
 present many obstacles to people who cannot operate a standard keyboard or
 mouse.
 <br />We do not want to support this social exclusion but rather see the support
 for handicapped persons as an important feature for every software. The KDE
 accessibility team knows about the problem and they are working on improving
 the accessibility of KDE.
 <br />In order to get a completely accessible system, applications that use other
 toolkits need to be accessible as well. In this sense KDE based assistive
 technologies need to interoperate well with applications using other toolkits
 and vice versa. We appreciate the foundation of the FSG Accessibility Workgroup
 and are very pleased to announce our participation.</q>
</p>

<p>
 (KDE statement on the Creation of the Free Standards Accessibility Workgroup,
 as discussed on the kde-promo and kde-accessibility mailing lists)
</p>

<h2 id="kdeap">About the KDE Accessibility Project</h2>
<p>
 The KDEAP is a small on-line community of developers and other volunteers
 dedicated to ensure that KDE is accessible to all users.
</p>
<p>
 KDE already contains a number of <a href="../features/">accessibility features</a>,
 and there are some <a href="../features/">assistive technologies</a> for KDE.
 We are working on further improving the accessibility of KDE in the next KDE versions.
 Some of our ongoing work is documented in our <a href="../developer/">developers' section</a>.
 Especially important will be <a href="atk.php">AT-SPI</a> support, which Trolltech has promised to implement for KDE 4.0.
</p>

<h2 id="fsg">About the FSG Accessibility Workgroup</h2>
<p>
 The goal for the Free Standards Accessibility Workgroup is to define a number of
 standards around accessibility. You can find more information on the
 <a href="http://accessibility.freestandards.org/">FSG Accessibility Workgroup site</a>.
</p>

<?php
  include_once ("footer.inc");
?>
