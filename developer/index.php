<?php
  $page_title = "Developers' Information";
  $site_root = "../";
  include "accessibility.inc";
  include "header.inc";
?>

<div id="quicklinks">[
 <a href="#kttsd">Text To Speech</a> |
 <a href="#atspi">AT-SPI</a> |
 <a href="#wordcompletion">Word Completion</a> |
 <a href="#keyboard">Keyboard Accessibility</a> ]
</div>

<h2 id="kttsd">Text To Speech</h2>
 <p>
 The KDE Text-To-Speech Daemon offers a D-Bus interface for applications using
 speech synthesis.  Any notifications can also be read out.
 </p>
   <ul><li><a href="http://techbase.kde.org/Development/Tutorials/Text-To-Speech">More information on KTTSD</a></li></ul>

<h2 id="atspi">AT-SPI</h2>
 <p>
 The biggest task of the KDEAP developments is the planning and
 implementation of AT-SPI support for KDE. AT-SPI (Assistive Technologies
 Service Provider Interface) is a cross-toolkit interprocess protocol that
 allows assistive technologies like screen readers, on-screen keyboards or
 braille device drivers to access all of the graphical user interfaces
 of an application, abstracting from toolkit dependent GUI objects.
 </p>

 <p>
  Introduction:
 </p>
  <ul>
   <li>
    <a href="introduction.php">KDE And Assistive Technologies</a>
   </li>
  </ul>

 <p>
  Documents describing the current situation:
 </p>
  <ul>
   <li>
    <a href="qt.php">Qt Accessibility Architecture</a>
   </li><li>
    <a href="atk.php">GNOME Accessibility Architecture (ATK and AT-SPI)</a>
   </li><li>
    <a href="comparision.php">A Comparision of the Two Architectures</a>
   </li><li>
    <a href="kde_widgets.php">Accessibility status of KDE widgets</a>
   </li>
  </ul>

 <p>
  Possible bridge architectures:
 </p>
  <ul>
   <li>
    <a href="bridge.php">Bridging Between the Two Architectures</a>
   </li><li>
    <a href="qtconnection.php">Connecting the Accessibility Bridge to Qt</a>
   </li>
  </ul>

 <p>
  Conclusion:
 </p>
  <ul>
   <li>
    <a href="kdea11y.php">Ideas For A KDE Accessibility Framework</a>
   </li>
  </ul>

<h2 id="wordcompletion">Word Completion</h2>
 <p>
 One of the features recently added to KMouth is word completion.
 Our plan is to turn this into a library for use in other applications.
 </p><p>
 The file format of the word lists is shared by the GNOME On-screen Keyboard
 (GOK). We also plan to define a standard for storing word lists and
 settings, so GOK and KMouth can benefit from words added
 in other applications and vice versa.
 </p>

<h2 id="keyboard">Keyboard Accessibility</h2>
 <p>
 Hardly any part of KDE is independent of accessibility issues. One very
 important aspect of accessibility for example are the key bindings.
 Some people cannot use a mouse, some cannot use a keyboard, some can use
 both with pain. Making KDE fully keyboard accessible does not rely on big
 architecture changes. If you are a developer, please make sure all the GUI
 elements of you programs are easily accessible by the keyboard.
 </p>

<?php
  include_once ("footer.inc");
?>

