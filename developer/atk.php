<?php
  $page_title = "GNOME Accessibility Architecture (ATK and AT-SPI)";
  $site_root = "../";
  include "accessibility.inc";
  include "header.inc";
?>

<p>
 This description of the GNOME Accessibility Architecture was written
 by Gunnar Schmi Dt as part of our
 <a href="index.php">development efforts</a> for interoperable accessibility solutions.
</p>

<div id="quicklinks">[
 <a href="#introduction">Introduction</a> |
 <a href="#architecture">An Overview of the Architecture</a> |
 <a href="#interface">An Overview of the Interface</a> |
 <a href="#coreclasses">Core Classes Within ATK and AT-SPI</a> |
 <a href="#specialized">Specialized Accessibility Interfaces</a> |
 <a href="#furtherinfo">Further Information</a> ]
</div>

<h2><a name="introduction">Introduction</a></h2>
<p>
 The GNOME Accessibility Architecture was designed for providing a
 standardized interface between assistive technologies (on the one side)
 and applications and the user's desktop (on the other side). Within this
 document we will concentrate on the application-part of the architecture.
</p><p>
 This part of consists of two layers. The more GNOME-specific layer is
 the GNOME Accessibility Implementation Library (GAIL) which bridges
 between the GNOME widgets and a toolkit independent interface, the
 Accessibility Toolkit (ATK). The other layer is a dynamically loaded
 library which bridges between ATK and the CORBA based Assistive
 Technology Service Provider Interface (AT-SPI). AT-SPI was developed
 for interoperability between different toolkits, and is currently
 used by GTK2, Java and OpenOffice.
</p><p>
 For non-GNOME applications it is possible to bridge to either ATK or AT-SPI.
 As both are similar to each other we will discuss both simultaneously.
</p>

<h2><a name="architecture">An Overview of the Architecture</a></h2>
<div style="text-align:center;padding:0.5em;">
 <img src="atspi.png"
      alt="Diagram for AT-SPI. Description follows" width="430" height="285" />
</div>
<p>
 The GNOME Accessibility Architecture distinguishes between AT-SPI aware
 applications, assistive technologies and one accessibility broker.
</p><p>
 AT-SPI aware applications are applications that offer information about
 their user interface via the AT-SPI protocol. This can be achieved by several
 ways: GNOME applications get AT-SPI support for free: The GTK+ toolkit they
 are based on optionally loads GAIL, which bridges between the GNOME widgets
 and ATK. A second library is used in order to bridge between ATK and AT-SPI.
 Mozilla does not use GTK+ but implements the ATK api directly within the
 application. Java applications may use the Java Accessibility Framework which
 directly bridges to AT-SPI. In the context of AT-SPI applications are often
 called servers.
</p><p>
 Assistive technologies are applications that are interested in requesting
 information about the user interfaces of AT-SPI aware applications (like
 for example a screen reader which needs what to speak) or in triggering
 actions in these applications (like an on-screen keyboard which needs to
 send keystrokes). In the context of assistive technologies are often called
 clients.
</p><p>
 The accessibility broker is a daemon that coordinates the communication
 between AT-SPI aware applications and assistive technologies. Each AT-SPI
 aware application registers with the broker in order to offer its information.
 Assistive technologies may add event listeners to the broker, so that they get
 informed when accessibility related information in any application changes.
</p>

<h2><a name="interface">An Overview of the Interface</a></h2>
<p>
 The basic idea for both ATK and AT-SPI is to provide an accessibility
 object for each user-visible element in the user interface. The basic
 class for these objects (Accessible within AT-SPI and AtkObject within ATK)
 only contains methods for information that is common for all widgets (as
 for example the state of the widget, relations to other widgets and -
 of course - references to child widgets and to the parent). Information
 that's existence is dependent on the type of the widget is covered by
 interfaces which might be implemented by the actual accessibility object.
</p><p>
 The names of the classes are slightly different in ATK and AT-SPI. However,
 in most cases they are similar to each other, so we will mention both names
 within the headlines but use only one of these names within the text.
</p>

<h2><a name="coreclasses">Core Classes Within ATK and AT-SPI</a></h2>
<h3>The Class Accessible / AtkObject</h3>
<p>
 The class Accessible is the base class for all accessibility handling related
 to a given widget. It contains methods for enquiring and changing important
 properties of the widget:
</p>
<ul>
 <li>The Name of the Widget</li>
 <li>A description for the widget</li>
 <li>The relation to other objects</li>
 <li>The role of the widget within the user interface</li>
 <li>The state of the widget</li>
</ul>
<p>
 Additionally it contains methods for handling notifications when properties
 of the widget change and methods for determining the parent and the childs
 of the widget.
</p>

<h3>The Class AtkUtil Within ATK</h3>
<p>
 The main purpose of the class AtkUtil is to provide methods for adding and
 removing event listeners and for enquiring the root AtkObject of the
 application and the name and version of the GUI toolkit.
</p><p>
 An application or toolkit that bridges to ATK needs to subclass AtkUtil and
 to re-implement these methods. For event notification it is necessary to
 manually notify each of the event listeners.
</p>

<h3>The Interface Application Within AT-SPI</h3>
<p>
 The interface Application within AT-SPI is similar to the class AtkUtil
 within ATK. As a subinterface of Accessible it is the root Accessible of
 the application. Additionally it contains methods for adding and removing
 event listeners and for enquiring the name and version of the GUI toolkit.
</p>

<h3>The Classes AtkRegistry and AtkObjectFactory Within ATK</h3>
<p>
 The two classes AtkRegistry and AtkObjectFactory do only exist within ATK.
 Their purpose is to create the AtkObject instances for the actual widgets
 as follows:
</p><p>
 For each widget type you create a subclass of AtkObjectFactory which knows
 how to create the instances for the given widget. This AtkObjectFactory
 needs to be registered with AtkRegistry so that ATK knows the factory.
</p><p>
 When creating an AtkObject for a given widget you do only need to ask
 the registry which factory to use. In order to decide which factory to
 use, AtkRegistry uses the GType of the widget. (Unfortunately this implies
 that your widgets are GObject-based. For QObject-based widgets we would
 need an other way for creating the AtkObject instances.)
</p>

<h3>The Interface Registry Within AT-SPI</h3>
<p>
 The interface Registry within AT-SPI must not be mistaken for the
 AtkRegistry class within ATK as its purpose is different. It is
 implemented on the accessibility broker. There are only three methods
 of interest for an AT-SPI aware application. Two of these need to be
 called for registering and deregistering the application. The third
 one is defined within the EventListener interface which Registry is
 derived from and needs to be called for event notification.
</p>

<h2><a name="specialized">Specialized Accessibility Interfaces</a></h2>
<h3>The Interface Action / AtkAction</h3>
<p>
 The Action interface should be implemented if the user can directly
 interact with the corresponding widget, as for example with buttons
 or scroll bars. It contains methods for enquiring the names and
 descriptions for (possibly multiple) actions of the widgets and for
 triggering these actions.
</p>

<h3>The Interface Component / AtkComponent</h3>
<p>
 The Component interface should be implemented for all widgets that have
 a screen representation. It contains methods for enquiring the screen
 position, adding and removing a focus handler and grabbing the focus.
</p>

<h3>The Interface Selection / AtkSelection</h3>
<p>
 The Selection interface should be implemented for all widgets whose child
 widgets can be selected. It contains methods for selecting and deselecting
 children and for enquiring the selection status of a given child or for
 enquiring the references to the selected children.
</p>

<h3>The Interface AtkDocument</h3>
<p>
 The AtkDocument interface should be implemented for widgets that are
 associated with the Document Object Model (DOM). It provides a mechanism
 for AT clients to access the DOM.
</p><p>
 Please note that AtkDocument is currently unused. It is ignored within the
 current bridge to AT-SPI as there is no standard way to provide access to an
 application's DOM tree. In future versions of AT-SPI this interface might
 become a useful and powerful facility in addition to the other interfaces.
</p><p>
 Please also note that the use of this interface within an assistive technology
 would imply that the assistive technology has some specialized knowledge of
 the semantics of the document type represented in the DOM tree. Therefor using
 a DOM tree makes only sense as an optional, alternative way for the assistive
 technology to access the document data.
</p>

<h3>The Interface Image / AtkImage</h3>
<p>
 The Image interface should be implemented for widgets that contain image or
 pixmap graphics. It contains methods for enquiring the screen position of the
 image and for enquiring or setting a textual description of the image.
</p>

<h3>The Interface StreamableContent / AtkStreamableContent</h3>
<p>
 The StreamableContent interface should be implemented for widgets that
 contain streamable content. It contains methods for enquiring the type
 of the stream and for accessing the stream itself.
</p><p>
 Please note that currently there is no assistive technology that makes use
 of this interface.
</p>

<h3>The Interface Table / AtkTable</h3>
<p>
 The Table interface should be implemented for widgets that order their
 children like cells within a table. It contains methods for enquiring
 row- and column headers, for enquiring a description of the contents of
 the table, and for translating between cell positions and child numbers.
</p>

<h3>The Interface Text / AtkText</h3>
<p>
 The Text interface should be implemented for widgets that contain textual
 contents. It contains methods for handling the cursor, enquiring and setting
 a selection, and enquiring single characters as well as the part of the text
 that is before or after a given offset.
</p><p>
 This interface does not contain methods for changing the text or for
 hyperlinks within the text as these are handled within the EditableText
 and Hypertext interfaces.
</p>

<h3>The Interface EditableText / AtkEditableText</h3>
<p>
 The EditableText interface should be implemented for widgets that contain
 editable textual contents. This implies that the Text interface is also
 implemented. It contains methods for setting the complete text, for
 inserting a given text at a given offset, for deleting some part of the
 text, and for the standard cut, copy and paste functions.
</p><p>
 This interface does not contain methods for enquiring text contents or for
 handling the cursor as these are handled within the Text interface.
</p>

<h3>The Interface Hypertext / AtkHypertext</h3>
<p>
 The Hypertext interface should be implemented for widgets that contain
 hypertext as their contents. In most cases this implies that the Text
 interface is also implemented. However, it is possible that an image
 implements this interface without implementing the Text interface if
 it does not have any textual information. The Hypertext interface contains
 methods for enquiring references to Hyperlink objects for the hyperlinks
 within the text.
</p><p>
 This interface does not contain methods for enquiring text contents or for
 handling the cursor as these are handled within the Text interface.
</p>

<h3>The Class Hyperlink / AtkHyperlink</h3>
<p>
 The Hyperlink class represents a single hyperlink within a hypertext. It
 contains methods for enquiring the anchors and the URIs of the hyperlink
 (The hyperlink might contain several anchors and several URIs if it is
 an image map.)
</p><p>
 For activating the links the Hyperlink class implements the Action interface.
</p><p>
 Please note that Hyperlink is the only specialized interface that is not
 returned by querying AtkObjects. Instead it is returned from calls to the
 Hypertext interface. Therefore objects that implement Hyperlink are not
 always Accessible/AtkObject instances.
</p>

<h3>The Interface Value / AtkValue</h3>
<p>
 The Value interface should be implemented for widgets that represent a
 value within a bounded range of values. It contains methods for enquiring
 the minimum, maximum and the current value and for changing the value.
 It is possible to set the Value interface to be read-only in which case any
 attempt to change the current value will fail.
</p>


<h2><a name="furtherinfo">Further Information</a></h2>
<p>
 You can find further information on the homepage of the
 (<a href="http://developer.gnome.org/projects/gap/"
    >GNOME Accessibility Project</a>).
 The GNOME site also has detailed information about the
 <a href="http://developer.gnome.org/doc/API/2.0/atk/book1.html"
   >classes and interfaces of ATK</a>
 and about the
 <a href="http://cvs.gnome.org/bonsai/rview.cgi?cvsroot=/cvs/gnome&amp;dir=at-spi/idl"
   >AT-SPI interfaces</a>. In the GNOME CVS you can also find the
   <a href="http://cvs.gnome.org/bonsai/rview.cgi?cvsroot=/cvs/gnome&amp;dir=gail/gail"
   >GNOME Accessibility Implementation Library</a>, which might serve
   as an example implementation for a bridge to ATK.
</p>

<?php
  include_once ("footer.inc");
?>
