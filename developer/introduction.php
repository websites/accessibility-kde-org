<?php
  $page_title = "KDE And Assistive Technologies";
  $site_root = "../";
  include "accessibility.inc";
  include "header.inc";
?>

<p>
 This introduction was written by Olaf Jan Schmidt as part of our
 <a href="index.php">development efforts</a> for interoperable accessibility solutions.
</p>

<div id="quicklinks">[
 <a href="#assistiveTechnologies">About Assistive Technologies</a> |
 <a href="#interfaces">Why Special Interfaces Are Needed</a> |
 <a href="#solutions">Existing Solutions</a> |
 <a href="#obstacles">Obstacles And Advantages of a Bridging Solution</a> |
 <a href="#apis">Why Native APIs Are Needed</a> ]
</div>

<h2 id="assistiveTechnologies">About Assistive Technologies</h2>
<p>
 Most applications are written for users capable of seeing the screen,
 and of using both a keyboard and a mouse in normal speed.
 For a number of people, this expectation creates huge obstacles, and
 in the last years, a lot of specialized assistive technologies have
 been written to allow a user to access the computer using braille devices,
 screen readers, on-screen keyboards, eye tracking devices, head pointing devices,
 two and three button devices, etc.
</p>

<h2 id="interfaces">Why Special Interfaces Are Needed</h2>
<p>
 A typical problem when developing these assistive technologies was that it is often
 very difficult to convert the graphical information on a screen into something
 that can be read out by a screen reader or spelled out on a braille display.
 And while specialized input devices can simulate a mouse or a keyboard, graphical
 user elements often behave in ways that make it tricky to find emulation techniques
 that work in all situations without problems.
</p>
<p>
 As a result of these problems, special interfaces for assisitive technologies have
 been created on different platforms. All GUI elements of an application
 can be accessed via these interfaces, so that all assistive technologies
 work with all applications and vice versa. This way, assisitive technologies need
 not try to emulate keyboards and mice or guess how a screen representation can be
 converted to pure text, but can rather directly access all GUI elements of an application
 and show them to the users in other, more accessible forms.
</p>

<h2 id="solutions">Existing Solutions</h2>
<p>
 The Windows version of Qt has support for an accessibility interface through the
 <a href="qt.php">Qt Accessibility Framework</a>, which is currently being extended
 by Trolltech to also include all functionality required by Mac and Unix
 solutions. This would allow Qt to forward all necessary information about GUI
 elements to the different accessibility solutions on Windows, Mac and Unix.
</p><p>
 On Unix, the cross-toolkit
 <a href="atk.php">AT-SPI (Assistive Technologies Service Provider Interface)</a>
 interprocess communication protocol is being used by GTK2, Java and OpenOffice.
 It was developed by the GNOME Accessibility Project as a way to connect GNOME's
 accessibility solutions with other toolkits. AT-SPI is very powerful and is
 the only existing solution for communication between accessibility aware
 applications and assistive technologies on Unix. Based on CORBA, it allows assistive
 technologies to access all of the graphical user interfaces of an application,
 abstracting from toolkit dependent GUI objects.
</p><p>
  Further information:
</p>
  <ul>
   <li>
    <a href="qt.php">Qt Accessibility Architecture</a>
   </li><li>
    <a href="atk.php">GNOME Accessibility Architecture (ATK and AT-SPI)</a>
   </li><li>
    <a href="comparision.php">A Comparision of the Two Architectures</a>
   </li>
  </ul>

<h2 id="obstacles">Obstacles And Advantages of a Bridging Solution</h2>
<p>
 From a KDE perspective, a bridge between Qt and AT-SPI has two problems and one huge
 benefit. The first problem are dependencies, as the AT-SPI registry currently
 requires GTK2+. It should be possible to reduce these dependencies to glib and ORBIT2,
 but this might perhaps require rewriting large parts of it. These would not be dependencies
 for KDE itself, but only for running assistive technologies.
</p><p>
 A second problem is the need to use CORBA in assistive technologies.
 This requirement could be avoided by offering a native KDE API through an accessibility
 library speaking CORBA, so assistive technology developers do not have to learn CORBA.
 Another solution that has been suggested is to move AT-SPI to a <a href="bridge.php#dbus">different IPC
 like D-BUS</a>, but this still needs to be evaluated.
</p><p>
 The benefit of a <a href="bridge.php">bridging solution</a> would be interoperability
 between assistive technologies, which we believe to be a crucial element of any interface
 for assistive technologies.
</p><p>
 Interfaces for assistive technologies only make sense if they are widely
 supported by several toolkits, so you can use one assistive technology to access
 all the applications you use. Otherwise you would end up with the need to install
 several braille device drivers in parallel, and to constantly switch between
 them, if that is technically possible at all. Or you would have to run several
 on-screen keyboards at the same time for accessing several applications written
 with different toolkits. Or the application you need won't run at all with the
 assistive technology you also need.
</p><p>
 For similar reasons, it is necessary that the interface contains all the
 functionality needed by different assistive technologies.
</p><p>
  Further information:
</p>
  <ul>
   <li>
    <a href="bridge.php">Bridging From Qt To AT-SPI</a>
   </li><li>
    <a href="qtconnection.php">Connecting the Accessibility Bridge to Qt</a>
   </li>
  </ul>

<h3 id="apis">Why Native APIs Are Needed</h3>
<p>
 Support for our accessibility effords is far more likely if the APIs used are not felt to be alien
 to KDE. This does not mean that we have to refuse neat cooperation and AT-SPI support for KDE,
 but it means that an API for assistive technologies needs to be created using Qt and KDE objects.
</p><p>
 The KDE Accessibility Project cannot implement a full accessibility solution without
 the help of the general KDE community. These documents aim at providing the necessary
 information to find an architecture that is accepted by the KDE core developers, Trolltech
 and those who have already implemented AT-SPI support. If any crucial ideas or arguments
 have been left out, please send a mail to the KDE accessibility mailing list so this website
 can be updated.
</p><p>
 We (as KDEAP) will only participate in effords that allow for bridging to AT-SPI.
 At the same time, we are aware that writing the necessary bridges might take a long time.
 It would be wrong to discourage direct use of the Qt Accessibility Architecture until the
 bridges are finished. But if future cooperation with other toolkits is not to be ruled
 out, a possible migration path to a later bridging solution must be possible. This can be
 accomplished by defining KDE APIs for assistive technologies that are close enough to
 AT-SPI to make later bridging as easy as possible.
</p><p>
  Further information:
</p>
  <ul>
   <li>
    <a href="kdea11y.php">Ideas For A KDE Accessibility Framework</a>
   </li>
  </ul>

<?php
  include_once ("footer.inc");
?>
