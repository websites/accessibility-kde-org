<?php
  $page_title = "Ideas For A KDE Accessibility Framework";
  $site_root = "../";
  include "accessibility.inc";
  include "header.inc";
?>

<p>
 This suggestion was written by Olaf Jan Schmidt as part of our
 <a href="index.php">development efforts</a> for interoperable accessibility solutions.
</p>

<div id="quicklinks">[
 <a href="#requirements">Requirements For a KDE Accessibility Framework</a> |
 <a href="#apps">Accessible Applications (AT servers)</a> |
 <a href="#ats">Assistive Technologies (AT clients)</a> |
 <a href="#roadmaps">Possible Roadmaps</a> ]
</div>

<h2><a name="requirements">Requirements For A KDE Accessibility Framework</a></h2>
<p>
 The KDE accessibility framework pursued by the KDE Accessibility Project
 has to meet the following requirements:
</p>
 <ul>
  <li>
   Native KDE/Qt APIs for both assistive technologies and accessible applications
  </li><li>
   No extra work for developers that use standard GUI elements and widgets
  </li><li>
   Only a little extra work for developers writing custom widgets
  </li><li>
   Reasonable amount of work for the people implementing the accessibility interfaces
  </li><li>
   A Qt based solution rather than one working only with KDE
  </li><li>
   Interoperability with other toolkits
  </li><li>
   Support for assistive technologies on Mac OS X and Windows once applications
   are ported to run natively on these platforms (via Qt)
  </li><li>
   No extra dependencies for KDE or Qt itself
  </li><li>
   Reasonable dependencies for the accessibility libraries
  </li>
 </ul>

<h2><a name="apps">Accessible Applications (AT servers)</a></h2>
<p>
 On the side of the accessible applications, a new API is not needed, as we can use
 <a href="qt.php">QAccessibleInterface</a> once it has been extended by
 Trolltech to contain all the functionality needed to make GUI elements fully
 accessible. Once the next Qt version has been released, KDE can start to write
 QAccessibleInterface objects for those KDE GUI elements that need customized accessibility
 implementations.
</p><p>
 In its current state, the Qt Accessibility Framework requires applications to provide
 a plug-in if they need to provide accessibility implementations of custom GUI elements.
 An easier API for applications providing only one or two custom widgets would be a method
 in each KWidget (or better QObject itself) that can be overloaded to return a subclass of
 QAccessibleInterface:
</p><p>
 <code>public *QAccessibleInterface QObject::accessibleInterface(); virtual;</code>
</p><p>
 The default implementation could search for an appropriate QAccessibleInterface in the
 loaded accessibility plug-ins.
</p><p>
  Further information:
</p>
  <ul>
   <li>
    <a href="qt.php">Qt Accessibility Architecture</a>
   </li><li>
    <a href="bridge.php">Bridging From Qt To AT-SPI</a>
   </li><li>
    <a href="qtconnection.php">Connecting the Accessibility Bridge to Qt</a>
   </li>
  </ul>

<h2><a name="ats">Assistive Technologies (AT clients)</a></h2>
<p>
 For assistive technologies, we can define an API that provides all needed functionality using Qt
 and KDE objects. While Trolltech is working on extending the accessibility features in Qt,
 and while bridges to AT-SPI are still in development, the easier, more basic parts
 of the API can be implemented using direct DCOP communication to a Qt plug-in. Once the
 server side bridge to AT-SPI has been implemented and all Qt applications can be reached
 via AT-SPI, the client side library can be fully implemented by bridging to AT-SPI.
</p><p>
 The API can be either written to match AT-SPI closely, or to match QAccessibleInterface closely.
 While using QAccessibleInterface looks like a nicer and cleaner solution at first glance, it has
 two important drawbacks: Not only that client side bridging is far more difficult, it would also
 mean that the definition of the API cannot be started before Trolltech has released the updated
 version of their accessibility framework.
</p><p>
 Matching the API to AT-SPI would also leave the opportunity open to move AT-SPI onto D-BUS or
 DCOP if this should find the support by the GNOME Accessibility Project.
</p><p>
  Further information:
</p>
  <ul>
   <li>
    <a href="qt.php">Qt Accessibility Architecture</a>
   </li><li>
    <a href="atk.php">GNOME Accessibility Architecture (ATK and AT-SPI)</a>
   </li><li>
    <a href="comparision.php">A Comparision of the Two Architectures</a>
   </li>
  </ul>

<h2><a name="roadmaps">Possible Roadmap</a></h2>
 <ol>
  <li>
   <ul>
    <li>
     Define an assistive technology library API similar to AT-SPI
    </li><li>
     Maybe write a prove-of-concept implemention using DCOP
     <br />(Assistive technologies using the API can now offer some functionality, but for Qt applications only)
    </li><li>
     Start to implement the assistive technology library
    </li>
   </ul>
  </li><li>
   <ul>
    <li>
     Trolltech releases the updated QAccessibleInterface
    </li>
   </ul>
  </li><li>
   <ul>
    <li>
     Implement the bridge from QAccessibleInterface to AT-SPI
     <br />(Non-KDE Assistive technologies are now fully functional with Qt applications)
    </li><li>
     Finish the assistive technology library
     <br />(Assistive technologies using the API are now fully functional with AT-SPI aware applications)
    </li>
   </ul>
  </li>
 </ol>

<?php
  include_once ("footer.inc");
?>
