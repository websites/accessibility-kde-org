<?php
  $page_title = "KTTS - KDE Text-to-Speech System";
  $site_root = "../../";
  include "accessibility.inc";
  include "header.inc";
?>

<p>The content that was here has moved to <a href="http://techbase.kde.org/Development/Tutorials/Text-To-Speech">Techbase</a></p>

<?php
  include_once ("footer.inc");
?>
