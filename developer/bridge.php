<?php
  $page_title = "Bridging Between the Two Architectures";
  $site_root = "../";
  include "accessibility.inc";
  include "header.inc";
?>

<p>
 This document is a draft overview of possible bridges for implementing
 AT-SPI support in Qt and KDE, written by Gunnar Schmi Dt as part of our
 <a href="index.php">development efforts</a> for interoperable accessibility solutions.
</p><p>
 <a href="atk.php">AT-SPI (Assistive Technologies Service Provider Interface)</a>
 is an interprocess communication protocol that allows
 assistive technologies to access all of the graphical user interfaces
 of an application, abstracting from toolkit dependent GUI objects.
 It was developed by the GNOME Accessibility Project, but it is based on CORBA and
 was written as a toolkit-independent protocol that is also used by Mozilla, OpenOffice
 and Java.
</p><p>
 In order to simplify the description we skip the discussion
 <a href="qtconnection.php">how the bridge will be connected to Qt</a>.
</p>

<div id="quicklinks">[
 <a href="#atk">Bridging to ATK</a> |
 <a href="#atspi">Bridging to AT-SPI</a> |
 <a href="#external">External Bridge</a> |
 <a href="#dbus">Move AT-SPI onto D-BUS</a> ]
</div>

<h2><a name="atk">Bridging to ATK</a></h2>
<div style="text-align:center;padding:0.5em;">
 <img src="atkbridge.png"
      alt="Diagram of this approach." width="480" height="348" />
</div>
<p>
 ATK is a glib-based in-process interface for accessibility information.
 A bridge between <a href="atk.php">ATK and AT-SPI</a> is used for GNOME
 and can be re-used by bridging to ATK.
</p><p>
 If we bridge via ATK, we need to convert between
 the Qt based <a href="qt.php">QAccessibleInterface classes</a>
 (either extended or combined with
 an access to the corresponding widgets) and the glib based AtkObject classes.
 In most cases this conversion is straight forward (just a conversion
 from Unicode-based QStrings to UTF8-based GStrings). The only exceptions are
 the interfaces AtkDocument and AtkStreamableContent. However, these two
 interfaces can safely be skipped in the first version of our bridge:
 AtkDocument is currently ignored in AT-SPI as there is no
 standard way to provide access to an application's DOM tree. And currently
 there is no assistive technology that makes use of AtkStreamableContent.
 Additionally bridging to AtkStreamableContent might become easier if
 KStreamer becomes the basis of a new media framework in KDE 4.0.
</p><p>
 As an estimate for the speed for this architecture we have to look on several
 details: The existing bridge between ATK and AT-SPI is straightforward. Reusing it
 will therefore probably be fast. The conversion within our bridge is an
 additional layer and its speed will depend on the amount of data we need
 to convert from qt to glib.
</p><p>
 In any case we do not need to cope with CORBA if we decide to go this way,
 so the implementation should be fairly easy.
</p>

<h2><a name="atspi">Bridging to AT-SPI</a></h2>
<div style="text-align:center;padding:0.5em;">
 <img src="atspibridge.png"
      alt="Diagram of this approach." width="480" height="285" />
</div>
<p>
 If we want to avoid a conversion between Qt and glib we might
 decide to bridge directly to AT-SPI. In this case we need to deal with CORBA,
 but would eliminate one extra bridge within the architecture.
</p><p>
 Depending on the speed of the CORBA implementation we decide to use we would
 be faster or slower than with a bridge to ATK. However, as I assume that the
 CORBA implementation that is used within GNOME is fast, we might only gain
 the speed that is otherwise lost by converting between Qt and glib.
</p><p>
 When implementing this architecture we use CORBA, which is complicated to use.
 Therefore it will take longer to implement a bridge to AT-SPI than to implement
 a bridge to ATK.
</p>

<h2><a name="external">External Bridge</a></h2>
<div style="text-align:center;padding:0.5em;">
 <img src="doublebridge.png"
      alt="Diagram of this approach." width="480" height="348" />
</div>
<p>
 If we want to avoid both a glib dependency and a CORBA
 dependency within the Qt Accessibility bridge we could design our own
 protocol using either MCOP, DCOP or D-BUS. However, in this case we would
 either loose the interoperability or we would need to write an external
 bridge to AT-SPI.
</p><p>
 As this external bridge needs to speak <strong>two</strong> different IPC
 protocols this bridge will be both heavy to write and maintain. The two
 bridges (the Qt Accessibility bridge and the external bridge) together
 will in any case result in the slowest architecture. Additionally it will
 take far more time to implement this architecture than to implement any
 of the other two architectures.
</p>

<h2><a name="dbus">Move AT-SPI onto D-BUS</a></h2>
<p>
 Maybe it is possibile to avoid bridging at all by porting AT-SPI onto D-BUS or DCOP.
 While this looks like an interesting idea from a KDE perspective, the developers involved
 in AT-SPI see it as very unlikely that D-BUS and DCOP are powerful enough to handle AT-SPI.
 CORBA supports to share objects between different applications, whereas D-BUS and DCOP
 are message passing protocols that support remote method invocation as well. It is not sure
 that the requirements of AT-SPI to the IPC can easily be emulated on top of D-BUS or DCOP.
</p><p>
 Changing AT-SPI requires great changes to the AT-SPI registry, ATK and the Java accessibility
 libraries. This would be far more work than dealing with CORBA in an optional KDE library.
 Alternatively, someone could write an external bridge, which would be heavy overhead and
 even more work.
</p>

<?php
  include_once ("footer.inc");
?>
