<?php
  $page_title = "Qt Accessibility Architecture";
  $site_root = "../";
  include "accessibility.inc";
  include "header.inc";
?>

<p>
 This description of the Qt Accessibility Architecture was written
 by Gunnar Schmi Dt as part of our
 <a href="index.php">development efforts</a> for interoperable accessibility solutions.
 Thanks to Jos&eacute; Pablo Ezequiel Fern&aacute;ndez for earlier work on
 the topic.
</p>

<div id="quicklinks">[
 <a href="#introduction">Introduction</a> |
 <a href="#architecture">An Overview of the Architecture</a> |
 <a href="#detailed">A More Detailed View on the Architecture</a> |
 <a href="#coreclasses">Details About the Core Classes</a> |
 <a href="#furtherinfo">Further Information</a> ]
</div>

<h2><a name="introduction">Introduction</a></h2>
<p>
 The Qt Accessibility Architecture was added to Qt for providing support for
 the Microsoft Active Accessibility (MSAA) technology on Windows. MSAA can
 be described as a part of the operating system that collects accessibility
 relevant information from MSAA aware applications and offers these
 information to assistive technologies.
</p><p>
 Unfortunately MSAA was not designed to be the only source of information
 that is used by assistive technologies. It rather complements existing
 technologies for determining what is on the screen (such as the Microsoft
 Screen Access Model (MSAM)).
</p><p>
 In order to design a technology similar to MSAA for Unix systems you have
 to start from scratch as there are no technologies as MSAM. As a result
 of that it will be neccessary to extend the Qt Accessibility Architecture
 to fit the extended requirements.
</p>

<h2><a name="architecture">An Overview of the Architecture</a></h2>
<p>
 The basic idea for the current design of the Qt Accessibility Architecture is
 to provide an accessibility object for all widgets within the application.
 However, only the base classes for these objects are part of the Qt library,
 the actual implementation of the accessibility information for the widgets
 is hidden in a plug-in.
</p><p>
 For widgets that are implemented outside of Qt (i.e., KDE widgets or
 application widgets) it is possible to install additional plug-ins.
</p>

<h2><a name="detailed">A More Detailed View on the Architecture</a></h2>
<div style="text-align:center;padding:0.5em;">
 <img src="qt.png" alt="Diagram for Qt Accessibility. Description follows" width="452" height="285" />
</div>
<p>
 The three classes of the Qt Accessibility Architecture that are part of
 the Qt library are QAccessible, QAccessibleInterface, and QAccessibleObject.
 QAccessible consists of two static methods and a number of enumerations.
 QAccessibleInterface is the most important class outside of the plug-in.
 It is derived from QAccessible and contains pure virtual methods for
 enquiring all available information. The QAccessibleObject class is
 inherited from QAccessibleInterface and implements part of the functionality.
 For this purpose it also has a reference to the QObject it represents.
</p><p>
 The plug-ins contain implementations for QAccessibleInterface for all
 Qt widgets. The base class for these implementations is QAccessibleWidget,
 from which all other classes of the plug-in are derived. Some of these
 derived classes have a reference to the corresponding Qt widget (as for
 example QAccessibleButton), others use the reference that is part of
 QAccessibleWidget (as for example QAccessibleText). QAccessibleWidget is
 inherited from QAccessibleObject.
</p><p>
 Unfortunately not all GUI elements within Qt are widgets. For example the
 entries within a QTable or a QListView are simple C++ classes. For these
 elements no QAccessibleInterface instance is created. Instead you will
 have to use the QAccessibleInstance of the parent widget for handling
 these elements.
</p><p>
 In order to inform the Qt Accessibility Architecture architecture about changes
 in the accessibility information the widgets have to call a static method
 within the QAccessible class.
</p>

<h2><a name="coreclasses">Details About the Core Classes</a></h2>
<h3>The QAccessible Class</h3>
<p>
 The QAccessible class simply contains a three static methods and a number of
 enumerations:
</p>
<ul>
 <li>
  <p>
   The method <code>static bool isActive()</code> returns true if accessibility
   information have been requested by an AT client.
  </p>
 </li> <li>
  <p>
   The method <code>static QRESULT queryAccessibleInterface (QObject *object,
   QAccessibleInterface **iface)</code> can be used to create a
   QAccessibleInterface instance for a given QObject. In order to do this, the
   method takes the class name of the object and checks which plug-in can
   handle the object. If no such plug-in is found, the method continues with
   the parent class and so on.
  </p><p>
   Please note that not all GUI elemants within Qt are inherited from QObject.
   For example the entries within a QTable or a QListView are simple C++
   classes, so you cannot use this method on these. Instead you need to use
   call this method for their parent widget.
  </p>
 </li><li>
  <p>
   The method <code>static void updateAccessibility (QObject *object,
   int control, Event reason)</code> needs to be called whenever the
   accessibility information changes.
  </p>
 </li><li>
  <p>
   The enumeration <code>Event</code> describes possible reasons for changes
   to the accessibility information.
  </p>
 </li><li>
  <p>
   The enumerations <code>State</code> and <code>Role</code> describe the
   different states and roles a GUI element can have.
  </p>
 </li><li>
  <p>
   The enumeration <code>NavDirection</code> is used for navigating between
   child elements of an object.
  </p>
 </li><li>
  <p>
   The enumeration <code>Text</code> defines different types of textual
   information (as for example the name, description and value of an object).
  </p>
 </li>
</ul>

<h3>The QAccessibleInterface Class</h3>
<p>
 The QAccessibleInterface class is designed to be the only contract between
 the accessibility implementation of the widgets and the bridge to the
 accessibility infrastructure within the system. It provides methods for
 accessing many relevant properties:
</p>
<ul>
 <li>
  <p>
   The method <code>pure virtual bool isValid() const</code> returns true if
   the data returned by the QAccessibleInterface class is valid (all returned
   pointers are valid etc.). It is implemented for all objects.
  </p>
 </li><li>
  <p>
   The methods <code>pure virtual int childCount() const</code>,
   <code>pure virtual QRESULT queryChild (int control, QAccessibleInterface
   **iface) const</code>, and <code>pure virtual QRESULT queryParent
   (QAccessibleInterface **iface) const</code> can be used to enquire
   QAccessibilityInterface objects for the children and for the parent.
   They are implemented for all objects.
  </p>
 </li><li>
  <p>
   The methods <code>pure virtual Role role (int control) const</code>
   and <code>pure virtual state (int control) const</code> can be used to
   enquire the role and the state of the object or one of its children.
   They are implemented for all objects.
  </p>
 </li><li>
  <p>
   The method <code>pure virtual int navigate (NavDirection direction, int
   startControl) const</code> can be used to navigate through the children
   of the object. It is implemented for all objects.
  </p>
 </li><li>
  <p>
   The method <code>pure virtual bool setFocus (int control) const</code> can
   be used to set the focus.
  </p>
 </li><li>
  <p>
   The methods <code>pure virtual int controlAt (int x, int y) const</code>,
   and <code>pure virtual QRect rect (int control) const</code> can be used to
   enquire screen coordinates for the objects. They are implemented for all
   visual objects.
  </p>
 </li><li>
  <p>
   The methods <code>pure virtual QMemArray&lt;int&gt; selection () const</code>,
   <code>pure virtual bool setSelected (int control, bool on, bool extend)</code>
   and <code>pure virtual clearSelection ()</code> can be used to handle the
   selection of child objects.
  </p>
 </li><li>
  <p>
   The methods <code>pure virtual QString text (Text t, int control) const</code>
   and <code>pure virtual setText (Text t, int control, const QString &amp;text)</code>
   can be used to enquire or set various textual informations of the object or
   one of its children.
  </p>
 </li><li>
  <p>
   The method <code>virtual bool doDefaultAction (int control)</code> can be
   used to trigger the default action of the object or one of its children.
  </p>
 </li>
</ul>

<h3>The QAccessibleObject Class</h3>
<p>
 This class is mainly provided for convenience. It is recommended that all
 subclasses of QAccessibleInterface use this class as their base class. It
 implements part of the QAccessibleInterface interface for QObjects.
</p>


<h2><a name="furtherinfo">Further Information</a></h2>
<p>
 In the 
 <a href="http://doc.trolltech.com/">Qt Reference Documentation</a>
 you can find details on the three classes
 <a href="http://doc.trolltech.com/3.2/qaccessible.html">QAccessible</a>,
 <a href="http://doc.trolltech.com/3.2/qaccessibleinterface.html">QAccessibleInterface</a>,
 and
 <a href="http://doc.trolltech.com/3.2/qaccessibleobject.html">QAccessibleObject</a>
</p>

<?php
  include_once ("footer.inc");
?>
