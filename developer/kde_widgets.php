<?php
  $page_title = "Accessibility status of KDE widgets";
  $site_root = "../";
  include "accessibility.inc";
  include "header.inc";
?>

<p>
The table below gives an overview of all widgets in KDE and the work that is either
needed or already done to provide accessibility support.
</p>
<table border="1">
<tr><th>Widget</th><th>Status</th></tr>
<tr><td>ExampleWidget1</td><td bgcolor="green">Ready.</td></tr>
<tr><td>ExampleWidget2</td><td bgcolor="yellow">Mostly done, still needs fooBar() call.</td></tr>
<tr><td>KActiveLabel</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KCharSelect</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KColorButton</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KColorCombo</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KComboBox</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KCModule</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KDateWidget</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KDatePicker</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KDialog</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KDualColorButton</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KEditListBox</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KFontCombo</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KFontChooser</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KFontRequester</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KGradientSelector</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KHistoryCombo</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KHSSelector</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KHtmlView</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KLed</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KListBox</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KListView</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KLineEdit</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KPasswordEdit</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KProgress</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KPushButton</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KKeyButton</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KRestrictedLine</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KIconButton</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KIconView</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KIntSpinBox</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KRuler</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KSqueezedTextLabel</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KTextBrowser</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KTextEdit</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KURLLabel</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KURLComboRequester</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KURLRequester</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KIntNumInput</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KDoubleNumInput</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KDoubleSpinBox</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KTimeWidget</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KDateTimeWidget</td><td bgcolor="red">Unknown</td></tr>
<tr><td>KDateTable</td><td bgcolor="red">Unknown</td></tr>
</table> 

<?php
  include_once ("footer.inc");
?>

