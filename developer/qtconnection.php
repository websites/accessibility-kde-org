<?php
  $page_title = "Connecting the Accessibility Bridge to Qt";
  $site_root = "../";
  include "accessibility.inc";
  include "header.inc";
?>

<p>
 This document describes how our planned
 <a href="bridge.php">accessibility bridge</a>
 can be connected to Qt. It was written by Gunnar Schmi Dt as part of our
 <a href="index.php">development efforts</a> for interoperable accessibility solutions.
 Thanks to Jos&eacute; Pablo Ezequiel Fern&aacute;ndez for earlier work on
 the topic.
</p><p>
 Trolltech have already implemented
 <a href="qt.php">Qt Accessibility classes</a>
 in their Qt versions for Windows. Trolltech offered to extend these if necessary
 for use on Unix.
</p><p>
 The bridge can be connected to Qt in three different ways: It can be either a
 part of Qt, a library that is used by Qt, or a plug-in.
</p>

<div id="quicklinks">[
 <a href="#part">The Qt Accessibility Bridge as a Part of Qt</a> |
 <a href="#library">The Qt Accessibility Bridge as a Dynamically Loaded Library</a> |
 <a href="#plugin">The Qt Accessibility Bridge as a Plug-In</a> ]
</div>

<h2><a name="part">The Qt Accessibility Bridge as a Part of Qt</a></h2>
<div style="text-align:center;padding:0.5em;">
 <img src="part.png"
      alt="Diagram of this approach." width="376" height="238" />
</div>
<p>
 If the Qt Accessibility Bridge is written as a part of Qt, each
 application that uses Qt will automatically load the bridge. This results
 in a number of arguments against using this approach: The size of Qt is
 blown up even when the bridge is not needed, each instability inside the
 bridge will automatically introduce instabilities inside all applications
 that use Qt, and each dependency of the bridge will automatically become
 a dependency of Qt.
</p>

<h2><a name="library">The Qt Accessibility Bridge as a Dynamically Loaded Library</a></h2>
<div style="text-align:center;padding:0.5em;">
 <img src="library.png"
      alt="Diagram of this approach." width="376" height="317" />
</div>
<p>
 If the Qt Accessibility Bridge is written as a library, we will
 have a clear separation between Qt and the bridge. Qt will need to depend
 on the bridge in order to offer the accessibility information. Therefore
 the bridge needs to avoid Qt dependencies unless we accept cyclic library
 dependencies.
</p><p>
 Unfortunately the QAccessible classes are based on Qt, so it would be
 neccessary to design a Qt-free API as interface between the bridge and Qt.
 This directly implies that we will have an additional bridge between the
 QAccessible classes and the Qt-free API. This additional bridge will be a
 part of Qt.
</p>

<h2><a name="plugin">The Qt Accessibility Bridge as a Plug-in</a></h2>
<div style="text-align:center;padding:0.5em;">
 <img src="plugin.png"
      alt="Diagram of this approach." width="376" height="313" />
</div>
<p>
 If the Qt Accessibility Bridge is written as a plug-in, we will
 also have a clear separation between Qt and the bridge. Qt will only have
 an optional run-time dependency on the plugin, and the plug-in will depend
 on Qt. This introduces a problem when Qt is statically linked: either the
 application will not be accessible or it would be neccessary to also
 statically link the bride into the application.
</p>

<?php
  include_once ("footer.inc");
?>
