<?php
  $page_title = "A Comparision of the Two Architectures";
  $site_root = "../";
  include "accessibility.inc";
  include "header.inc";
?>

<p>
 This document describes which of the features of <a href="atk.php">ATK and
 AT-SPI</a> are also available in the <a href="qt.php">Qt Accessibility
 Architecture</a>, which can be emulated with simple code and which are
 completely missing. It was written by Gunnar Schmi Dt as part of our
 <a href="index.php">development efforts</a> for interoperable accessibility solutions.
</p>

<div id="quicklinks">[
 <a href="#overview">An Overview of the Missing Parts in Qt</a> |
 <a href="#details">Details Sorted by Interfaces</a> ]
</div>

<h2 id="overview">An Overview of the Missing Parts in Qt</h2>
<p>
 There are a number of features that are available within ATK and AT-SPI
 but are not available in the current QAccessibleInterface class. These
 features include:
</p><ul>
 <li>Methods for enquiring the relationship between several elements.</li>
 <li>Support for several actions, each connected with a name, a
     description, and a key binding. QAccessibleInterface does only
     allow one default action.</li>
 <li>Methods for enquiring the layer or the z-order of a GUI element.</li>
 <li>A method for selecting all children of an element.</li>
 <li>Methods for enquiring the position the size and a description for images.</li>
 <li>Methods for handling streamable content.</li>
 <li>Methods for enquiring accessibility relevant information in tables.
     These include:
     <ul>
      <li>A description for the table and for each row and each column,</li>
      <li>Methods for enquiring headers of the rows and columns,</li>
      <li>A method for requesting the child at a specified cell in the table, and</li>
      <li>A method for enquiring the row and column of a given child.</li>
     </ul></li>
 <li>Methods for handling a cursor and text attributes in elements with
     textual information.</li>
 <li>Methods for accessing the clipboard (e.g., cut, copy and past methods)
     in elements with editable textual contents.</li>
 <li>Methods for handling hyperlinks.</li>
 <li>Methods for handling elements that only display a value of a given
     range of numbers.</li>
</ul>

<h2 id="details">Details Sorted by Interfaces</h2>
<p>
 Within the following tables the first column specifies the methods
 that are available in ATK and AT-SPI. Unfortunately ATK and AT-SPI
 do not always have the same names. Therefor I chose to use the name
 that is used in ATK after stripping the interface name. For example
 the method &quot;<code>atk_object_get_name</code>&quot; in ATK is called
 &quot;get_name&quot; within the tables of this document.
</p><p>
 The second column specifies whether the actual method of a given row is
 available in ATK or AT-SPI (or in both). From the semantics of the
 methods that are only available for either ATK or AT-SPI I suppose that
 these do not need to be supported by our bridge.
</p><p>
 The third column specifies whether the method of a given row is supported
 by the Qt Accessibility Architecture. A &quot;yes&quot; in this column
 implies that there is a method for exactly this purpose in the
 QAccessibleInterface class. An entry &quot;emulatable&quot; is used for
 methods that can be emulated with other methods of the architecture
 (this includes access to the static <code>QAccessible::updateAccessibility</code> method
 and to the QAccessibleInterface, QAccessibleObject and QObject classes).
 A &quot;partially&quot; in the third column implies that the needed
 functionality is only partly availabe, and finally a &quot;no&quot; implies
 that it is completely missing.
</p><p>
 For all methods that contain a &quot;emulatable&quot; or a
 &quot;partially&quot; in the third column some comments are added below
 the table of the corresponding interface.
</p>

<h3>The Class Accessible/AtkObject</h3>
<table border="1" summary="This table lists for every function of Accessible and AtkObject whether there is a Qt counterpart.">
<tr><th>Method</th><th>Defined in</th><th>Available</th></tr>
<tr><td>get_name();</td>                                 <td>AT-SPI and ATK</td><td>yes</td></tr>
<tr><td>get_description();</td>                          <td>AT-SPI and ATK</td><td>yes</td></tr>
<tr><td>get_parent();</td>                               <td>AT-SPI and ATK</td><td>yes</td></tr>
<tr><td>get_n_accessible_children();</td>                <td>AT-SPI and ATK</td><td>yes</td></tr>
<tr><td>ref_accessible_child (index);</td>               <td>AT-SPI and ATK</td><td>yes</td></tr>
<tr><td>ref_relation_set();</td>                         <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_role();</td>                                 <td>AT-SPI and ATK</td><td>yes</td></tr>
<tr><td>ref_state_set();</td>                            <td>AT-SPI and ATK</td><td>yes</td></tr>
<tr><td>get_index_in_parent();</td>                      <td>AT-SPI and ATK</td><td>emulatable</td></tr>
<tr><td>set_name (*name);</td>                           <td>AT-SPI and ATK</td><td>emulatable</td></tr>
<tr><td>set_description (*description);</td>             <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>is_equal(obj);</td>                              <td>AT-SPI</td><td></td></tr>
<tr><td>get_role_name();</td>                            <td>AT-SPI</td><td></td></tr>
<tr><td>set_parent (*parent);</td>                       <td>ATK</td><td></td></tr>
<tr><td>set_role (role);</td>                            <td>ATK</td><td></td></tr>
<tr><td>connect_property_change_handler (*handler);</td> <td>ATK</td><td></td></tr>
<tr><td>remove_property_change_handler (handler_id);</td><td>ATK</td><td></td></tr>
<tr><td>notify_state_change (state, value);</td>         <td>ATK</td><td></td></tr>
<tr><td>initialize (gpointer data);</td>                 <td>ATK</td><td></td></tr>
<tr><td>add_relationship (relationship, *target);</td>   <td>ATK</td><td></td></tr>
<tr><td>remove_relationship (relationship, *target);</td><td>ATK</td><td></td></tr>
</table>
<p>
 The method <code>get_index_in_parent()</code> can be emulated
 by a loop over the children of the parent widget.
</p>

<h3>The Interface Action/AtkAction</h3>
<table border="1" summary="This table lists for every function of Action and AtkAtkAction whether there is a Qt counterpart.">
<tr><th>Method</th><th>Defined in</th><th>Available</th></tr>
<tr><td>boolean do_action (index);</td>           <td>AT-SPI and ATK</td><td>partially</td></tr>
<tr><td>get_n_actions()</td>                      <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_description (index);</td>             <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_name (index);</td>                    <td>AT-SPI and ATK</td><td>partially</td></tr>
<tr><td>get_key_binding (index);</td>             <td>AT-SPI and ATK</td><td>partially</td></tr>
<tr><td>get_localized_name (index);</td>          <td>ATK</td><td></td></tr>
<tr><td>set_description (index, description);</td><td>ATK</td><td></td></tr>
</table>
<p>
 The QAccessibleInterface class only allows to trigger a default action.
 Both ATK and AT-SPI allow to have multiple actions, all connected with a
 name, a description, and a key binding.
</p>

<h3>The Interface Component/AtkComponent</h3>
<table border="1" summary="This table lists for every function of Component and AtkComponent whether there is a Qt counterpart.">
<tr><th>Method</th><th>Defined in</th><th>Available</th></tr>
<tr><td>add_focus_handler (handler);</td>                      <td>AT-SPI and ATK</td><td>emulatable</td></tr>
<tr><td>contains (x, y, coord_type);</td>                      <td>AT-SPI and ATK</td><td>emulatable</td></tr>
<tr><td>get_extents (*x, *y, *width, *height, coord_type);</td><td>AT-SPI and ATK</td><td>yes</td></tr>
<tr><td>get_layer();</td>                                      <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_mdi_zorder();</td>                                 <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_position (*x,*y, coord_type);</td>                 <td>AT-SPI and ATK</td><td>yes</td></tr>
<tr><td>get_size (*width, *height);</td>                       <td>AT-SPI and ATK</td><td>yes</td></tr>
<tr><td>grab_focus ();</td>                                    <td>AT-SPI and ATK</td><td>yes</td></tr>
<tr><td>ref_accessible_at_point (x, y, coord_type);</td>       <td>AT-SPI and ATK</td><td>yes</td></tr>
<tr><td>remove_focus_handler (handler_id);</td>                <td>AT-SPI and ATK</td><td>emulatable</td></tr>
<tr><td>set_extents (x, y, width, height, coord_type);</td>    <td>ATK</td><td></td></tr>
<tr><td>set_position (x, y, coord_type);</td>                  <td>ATK</td><td></td></tr>
<tr><td>set_size (width, height);</td>                         <td>ATK</td><td></td></tr>
</table>
<p>
 The QAccessibleInterface class does not allow to add focus handlers.
 However, this can be emulated by some code within the
 <code>QAccessible::updateAccessibility (*object, control, reason)</code> method.
</p><p>
 The method <code>contains (x, y, coord_type)</code> can be emulated for example by
 asking for the screen extends and applying some geometric calculations.
</p>

<h3>The Interface Selection / AtkSelection</h3>
<table border="1" summary="This table lists for every function of Selection and AtkSelection whether there is a Qt counterpart.">
<tr><th>Method</th><th>Defined in</th><th>Available</th></tr>
<tr><td>add_selection (index);</td>    <td>AT-SPI and ATK</td><td>yes</td></tr>
<tr><td>clear_selection();</td>        <td>AT-SPI and ATK</td><td>yes</td></tr>
<tr><td>ref_selection (index);</td>    <td>AT-SPI and ATK</td><td>yes</td></tr>
<tr><td>get_selection_count();</td>    <td>AT-SPI and ATK</td><td>yes</td></tr>
<tr><td>is_child_selected (index);</td><td>AT-SPI and ATK</td><td>emulatable</td></tr>
<tr><td>remove_selection  (index);</td><td>AT-SPI and ATK</td><td>yes</td></tr>
<tr><td>select_all_selection();</td>   <td>AT-SPI and ATK</td><td>emulatable</td></tr>
</table>
<p>
 The method <code>is_child_selected (index)</code> can be emulated by asking the child
 widget for its state and then checking if the selected state is set. The
 method <code>select_all_selection()</code> can be emulated by sequentially selecting
 all children.
</p>

<h3>The Interface AtkDocument</h3>
<table border="1" summary="This table lists for every function of AtkDocument whether there is a Qt counterpart.">
<tr><th>Method</th><th>Defined in</th><th>Available</th></tr>
<tr><td>get_document_type();</td><td>ATK</td><td>no</td></tr>
<tr><td>get_document ();    </td><td>ATK</td><td>no</td></tr>
</table>
<p>
 Please note that AtkDocument is currently ignored within the current bridge
 to AT-SPI as there is no standard way to provide access to an application's
 DOM tree.
</p>

<h3>The Interface Image / AtkImage</h3>
<table border="1" summary="This table lists for every function of Image and AtkImage whether there is a Qt counterpart.">
<tr><th>Method</th><th>Defined in</th><th>Available</th></tr>
<tr><td>get_image_description();</td>                <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_image_position (*x, *y, coord_type);</td><td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_image_size (*width, *height);</td>       <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_image_extents (*x, *y, *width, *height, coord_type);</td><td>AT-SPI</td><td></td></tr>
<tr><td>set_image_description (description);</td>    <td>ATK</td><td></td></tr>
</table>

<h3>The Interface StreamableContent / AtkStreamableContent</h3>
<table border="1" summary="This table lists for every function of StreamableContent and AtkStreamableContent whether there is a Qt counterpart.">
<tr><th>Method</th><th>Defined in</th><th>Available</th></tr>
<tr><td>get_n_mime_types();</td>     <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_mime_type (index);</td>  <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_stream (*mime_type);</td><td>AT-SPI and ATK</td><td>no</td></tr>
</table>
<p>
 Please note that currently there is no assistive technology that makes use
 of this interface.
</p>

<h3>The Interface Table / AtkTable</h3>
<table border="1" summary="This table lists for every function of Table and AtkTable whether there is a Qt counterpart.">
<tr><th>Method</th><th>Defined in</th><th>Available</th></tr>
<tr><td>ref_at (row, column);</td>                        <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_index_at (row, column);</td>                  <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_column_at_index (index);</td>                 <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_row_at_index (index);</td>                    <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_n_columns();</td>                             <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_n_rows();</td>                                <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_column_extent_at (row, column);</td>          <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_row_extent_at (row, column);</td>             <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_caption();</td>                               <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_column_description (column);</td>             <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_row_description (row);</td>                   <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_column_header (column);</td>                  <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_row_header (row);</td>                        <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_summary();</td>                               <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_selected_columns (**selected);</td>           <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_selected_rows (**selected);</td>              <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>is_column_selected (column);</td>                 <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>is_row_selected (row);</td>                       <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>is_selected (row, column);</td>                   <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>add_column_selection (column);</td>               <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>add_row_selection (row);</td>                     <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>remove_column_selection (column);</td>            <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>remove_row_selection (row);</td>                  <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>set_caption (caption);</td>                       <td>ATK</td><td></td></tr>
<tr><td>set_row_description (row, description);</td>      <td>ATK</td><td></td></tr>
<tr><td>set_column_description (column, description);</td><td>ATK</td><td></td></tr>
<tr><td>set_row_header (row, header);</td>                <td>ATK</td><td></td></tr>
<tr><td>set_column_header (column, header);</td>          <td>ATK</td><td></td></tr>
<tr><td>set_summary (summary);</td>                       <td>ATK</td><td></td></tr>
</table>
<p>
 Please note that QAccessibleInterface provides a method that
 allows to navigate between the children of an element (e.g., to ask
 which child is above, below, or in the right or left of an other
 child). However, this method does not at all cover the requirements
 for the Table interface.
</p>

<h3>The Interface Text / AtkText</h3>
<table border="1" summary="This table lists for every function of Text and AtkText whether there is a Qt counterpart.">
<tr><th>Method</th><th>Defined in</th><th>Available</th></tr>
<tr><td>get_text (start_offset, end_offset);</td>                                       <td>AT-SPI and ATK</td><td>yes</td></tr>
<tr><td>get_character_at_offset (offset);</td>                                          <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_text_after_offset (offset, boundary_type, *start_offset, *end_offset);</td> <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_text_at_offset (offset, boundary_type, *start_offset, *end_offset);</td>    <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_text_before_offset (offset, boundary_type, *start_offset, *end_offset);</td><td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_caret_offset();</td>                                                        <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_character_extents (offset, *x, *y, *width, *height, coords);</td>           <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_run_attributes (offset, *start_offset, *end_offset);</td>                   <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_character_count();</td>                                                     <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_offset_at_point (x, y, coords);</td>                                        <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_n_selections();</td>                                                        <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_selection (selection_num, *start_offset, *end_offset);</td>                 <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>add_selection (start_offset, end_offset);</td>                                  <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>remove_selection (selection_num);</td>                                          <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>set_selection (selection_num, start_offset, end_offset);</td>                   <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>set_caret_offset (offset);</td>                                                 <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_range_extents (start_offset, end_offset, coord_type, *rect)</td>            <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_bounded_ranges (*rect, coord_type, x_clip_type, y_clip_type)</td>           <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_default_attributes();</td>                                                  <td>ATK</td><td></td></tr>
</table>

<h3>The Interface EditableText / AtkEditableText</h3>
<table border="1" summary="This table lists for every function of EditableText and AtkEditableText whether there is a Qt counterpart.">
<tr><th>Method</th><th>Defined in</th><th>Available</th></tr>
<tr><td>set_run_attributes (attrib_set, start_offset, end_offset);</td><td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>set_text_contents (string);</td>                               <td>AT-SPI and ATK</td><td>yes</td></tr>
<tr><td>insert_text (string, length, *position);</td>                  <td>AT-SPI and ATK</td><td>emulatable</td></tr>
<tr><td>copy_text (start_pos, end_pos);</td>                           <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>cut_text (start_pos, end_pos);</td>                            <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>delete_text (start_pos, end_pos);</td>                         <td>AT-SPI and ATK</td><td>emulatable</td></tr>
<tr><td>paste_text (position);</td>                                    <td>AT-SPI and ATK</td><td>no</td></tr>
</table>
<p>
 The methods <code>insert_text(...)</code> and <code>delete_text(...)</code> can be emulated by
 requesting the text, changing it, and setting the result. All other
 missing methods either need acces to the cursor position or access to
 the clipboard.
</p>

<h3>The Interface Hypertext / AtkHypertext</h3>
<table border="1" summary="This table lists for every function of Hypertext and AtkHypertext whether there is a Qt counterpart.">
<tr><th>Method</th><th>Defined in</th><th>Available</th></tr>
<tr><td>get_link (link_index);</td>      <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_n_links ();</td>             <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_link_index (char_index);</td><td>AT-SPI and ATK</td><td>no</td></tr>
</table>

<h3>The Class Hyperlink / AtkHyperlink</h3>
<table border="1" summary="This table lists for every function of Hyperlink and AtkHyperlink whether there is a Qt counterpart.">
<tr><th>Method</th><th>Defined in</th><th>Available</th></tr>
<tr><td>get_uri (index);</td>   <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_object (index);</td><td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_end_index();</td>   <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_start_index();</td> <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>is_valid();</td>        <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_n_anchors();</td>   <td>AT-SPI and ATK</td><td>no</td></tr>
</table>
<p>
 Please note that the support for class is tightly connected to
 the support of the Hypertext interface.
</p>

<h3>The Interface Value / AtkValue</h3>
<table border="1" summary="This table lists for every function of Value and AtkValue whether there is a Qt counterpart.">
<tr><th>Method</th><th>Defined in</th><th>Available</th></tr>
<tr><td>get_current_value (*value);</td><td>AT-SPI and ATK</td><td>emulatable</td></tr>
<tr><td>get_maximum_value (*value);</td><td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_minimum_value (*value);</td><td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>set_current_value (value);</td> <td>AT-SPI and ATK</td><td>no</td></tr>
<tr><td>get_minimum_increment (*value);</td><td>AT-SPI</td><td>no</td></tr>
</table>
<p>
 Please note that QAccessibleInterface has a method that can be used for
 enquiring the value of any element. However, that method always returns
 a QString, i.e., numers for example are converted to text. As the Value
 interface needs to return numbers we would need to convert the QString
 back to its original type.
</p>

<?php
  include_once ("footer.inc");
?>
