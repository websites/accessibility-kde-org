<?php
$page_title = "KDE Accessibility Project";
$site_root = "./";
include "accessibility.inc";
include "header.inc";
?>

<p>New information about KDE Accessibility can be found on the KDE wikis.</p>
<h3><a href="http://userbase.kde.org/Applications/Accessibility/">Information for users</a></h3>
<p>Users looking for how to use KDE Accessibility applications, what features are available and so on should look here.</p>
<h3><a href="http://community.kde.org/Accessibility">Accessibility information on the KDE wikis</a></h3>
<p>Developers looking to use KDE Accessibility technology in their applications or help develop Accessibility solutions
 check here.</p>

<?php
include "footer.inc";
?>
