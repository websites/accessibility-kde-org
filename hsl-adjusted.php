<?php
$page_title = "Hue, Saturation and Lightness of sRGB Colors";
$site_root = "./";
include "accessibility.inc";
include "header.inc";
?>

<style type="text/css"><!--
sub, sup { font-size:75% }
//--></style>

<p>
This page defines the sHSL color space, which is based on sRGB and
can be used to answer questions such as these:
</p>

<p>
 1. How big is the contrast between light green (#66FF00) and violet (#6600FF)?
(<a href="oxygen.php?color=66FF00,6600FF">Just sufficient, but not perfect</a>)
</p>

<p>
 2. How do we reduce the saturation of blue (#0000FF)
 while keeping the same hue and brightness?
(<a href="oxygen.php?color=0000FF,3838C0">The wanted color is #3838C0</a>)
</p>

<p>
 3. Which shade of yellow has the same brightness and saturation as blue (#0000FF)?
(<a href="oxygen.php?color=0000FF,505000">Dark yellow #505000</a>)
</p>

<div id="quicklinks"> [ 
  <a href="#introduction">Introduction</a> |
  <a href="#luminosity">Luminosity (Lightness) and Contrast</a> |
  <a href="#hue">Hue</a> |
  <a href="#saturation">Saturation</a> |
  <a href="#back">Conversion from sHSL to sRGB</a> |
  <a href="#changeluminosity">Changing the Luminosity of a Color</a> |
  <a href="#changesaturation">Changing the Saturation of a Color</a> |
  <a href="#changehue">Changing the Hue of a Color</a> ]
</div>

<h2><a name="introduction" />Introduction</h2>

<p>
In RGB representation, colors are described with three numbers for the <i>red</i>,
<i>green</i> and <i>blue</i> components. Different devices often
show slightly different colors for the same values. To solve
this problem, the <a href="http://www.w3.org/Graphics/Color/sRGB">sRGB</a>
(standard RGB) color space has been defined. sRGB is widely supported
by digital devices such as scanners, webcams, HDTV screens,
projectors, cameras, photo CDs and reprint machines.
It is also used in many standards (e.g. HTML, SVG, ODF).
</p>

<p>Changes in one of the three components influence the hue, the
saturation and the brightness of the color. Hence, it is almost
impossible to compare and change colors in RGB representation.
To compute alterations a color, we need to first calculate the hue,
saturation and lightness of the color and then to convert
the changed values back to sRGB.
</p>

<p>
The most commonly used alternative to RGB is called HSV or HSB
(<i>hue</i>, <i>saturation</i> and <i>brightness</i>/<i>value</i>).
HSV/HSB is unsuitable for our purpose, because the <i>brightness</i>
component does not represent the real brightness of the color. The real
brightness is also influenced by the <i>hue</i> and <i>saturation</i>:
</p>

<p>
<img alt="HSV color space" src="hsv.png"/>
</p>

<p>
A third popular color space is called HSL (<i>hue</i>, <i>saturation</i> and
<i>lightness</i>/<i>luminosity</i>). It has the advantage that
the <i>saturation</i> component is separated from the brightness,
but the simple RGB to HSL conversion algorithm used by most applications
still has problems. For example, the <i>hue</i> component still influences
the brightness of the color:
</p>

<p>
<img alt="HSL color space" src="hsl.png"/>
</p>

<p>
It is possible to define a correct conversion from sRGB to HSL.
To distinguish the color space defined like this from all other possible
HSL color spaces, we name it <i>sHSL</i> in allusion to sRGB.
</p>

<p>
The conversion from sRGB to sHSL is more difficult but it leads to much better results:
</p>

<p>
<img alt="HSV color space with hue-adjusted lightness" src="hsl-adjusted.png"/>
</p>

<h2><a name="luminosity" />Luminosity (Lightness) and Contrast</h2>

<p>
The sRGB color space defines the <i>red</i> (R), <i>green</i> (G)
and <i>blue</i> (B) components as numbers between 0 and 255.
It also has a gamma value of 2.2, which means that the components
can be normalized into a range from 0 to 1 (inclusive)
like this:
</p>

<p><code>
N<sub>r</sub>=(R/255)<sup><span style="display:none">^</span>2.2</sup>,
N<sub>g</sub>=(G/255)<sup><span style="display:none">^</span>2.2</sup>,
N<sub>b</sub>=(B/255)<sup><span style="display:none">^</span>2.2</sup>
</code></p>

<p>
The <i>luminosity</i> (L) of a color can then be calculated as a number between 0 (for black)
and 1 (for white)</a>:
</p>

<p><code>
L = f<sub>r</sub>&middot;N<sub>r</sub> + f<sub>g</sub>&middot;N<sub>g</sub> + f<sub>b</sub>&middot;N<sub>b</sub>
</code></p>

<p>
using the factors
</p>

<p><code>
f<sub>r</sub>=0.2126,
f<sub>g</sub>=0.7152,
f<sub>b</sub>=0.0722
</code></p>

<p>
This formula is taken from the document "<a href="http://www.w3.org/TR/UNDERSTANDING-WCAG20/Overview.html#visual-audio-contrast-contrast">Understanding WCAG 2.0</a>"
that is released by W3C together with the Web Content Accessibility Guidelines (WCAG 2.0).
It uses factors from the <a href="http://www.w3.org/Graphics/Color/sRGB">sRGB standard</a>
which in turn are based on an <a href="http://www.itu.int/rec/R-REC-BT.709/">ITU standard for HDTV</a>.
Most modern computer screens use similar factors.

<p>
"Understanding WCAG 2.0" also describes how to evaluate the contrast between two colors
(X and Y).
The <i>luminosity contrast ratio</i> (&Delta;) has a range of 1 to 21. 
It needs to be bigger than 5 for sufficient contrast and bigger than
10 for good contrast.
</p>

<p><code>
&Delta; = (L<sub>X</sub> + 0.05) / (L<sub>Y</sub> + 0.05)
</p></code>

<h2><a name="hue" />Hue</h2>

<p>
We now assign r, g and b one of the numbers 0, 1 and -1 each so that
</p>

<p><code>
N<sub>1</sub> = max { N<sub>r</sub>, N<sub>g</sub>, N<sub>b</sub> }
<br/>N<sub>-1</sub> = min { N<sub>r</sub>, N<sub>g</sub>, N<sub>b</sub> }
<br/>r+g+b=0
<code></p>

<p>
The common definition for the <i>hue</i> (H) as a number between 0 (inclusive)
and 360 (exclusive) can then be written as:
</p>

<p><code>
For N<sub>1</sub> = N<sub>-1</sub>: &nbsp; H = 0
<br/>For N<sub>b</sub> = N<sub>-1</sub> &lt; N<sub>1</sub>:
&nbsp; H = (N<sub>g</sub>-N<sub>r</sub>)/(N<sub>1</sub>-N<sub>-1</sub>)&middot;60
<br/>For N<sub>r</sub> = N<sub>-1</sub> &lt; N<sub>1</sub>:
&nbsp; H = (N<sub>b</sub>-N<sub>g</sub>)/(N<sub>1</sub>-N<sub>-1</sub>)&middot;60 + 120
<br/>For N<sub>g</sub> = N<sub>-1</sub> &lt; N<sub>1</sub>:
&nbsp; H = (N<sub>r</sub>-N<sub>b</sub>)/(N<sub>1</sub>-N<sub>-1</sub>)&middot;60 + 240
</code></p>

<h2><a name="saturation" />Saturation</h2>

<p>
The <i>saturation</i> (S) is a number between 0 (for gray, white and black)
and 1 (for full saturation).
</p>

<p><code>
For L = 0: &nbsp; S = 0
<br/>For L = 1: &nbsp; S = 0
<br/>For 0&lt;L&lt;1: &nbsp; S = max { (L-N<sub>-1</sub>)/L , (N<sub>1</sub>-L)/(1-L) }
</code></p>

<p>
The mathematical derivation from the definitions of <i>hue</i> and <i>luminosity</i>
to this simple formula is quite long and is therefore not shown on this page.
</p>

<h2><a name="back" />Conversion from sHSL to sRGB</h2>

<p>
The first step is to calculate some hue-dependent variables. 
</p>

<p><code>
r = |H - H mod 60 - 150| / 60 - 1.5
<br/>g = |H - H mod 60 + 110| / 60 - 1.5
<br/>b = |H - H mod 60 - 30| / 60 - 1.5
<br/>h = 1 - |(H mod 120)/60 - 1|
<br/>m = f<sub>1</sub> + h&middot;f<sub>0</sub> &nbsp; (using f<sub>r</sub>,f<sub>g</sub>,f<sub>b</sub> from above)
</code></p>

<p>
We now differenciate between two cases.
</p>

<p><code>
For 0 &le; L &le; m:
<br/>N<sub>1</sub> = L + L&middot;S&middot;(1-m)/m
<br/>N<sub>0</sub> = L + L&middot;S&middot;(h-m)/m
<br/>N<sub>-1</sub> = L - L&middot;S
</code></p>

<p><code>
For m &lt; L &le; 1:
<br/>N<sub>1</sub> = L + (1-L)&middot;S
<br/>N<sub>0</sub> = L + (1-L)&middot;S&middot;(h-m)/(1-m)
<br/>N<sub>-1</sub> = L - (1-L)&middot;S&middot;m/(1-m)
</code></p>

<p>
The last step is to de-normalize the values for red, green and blue:
</p>

<p><code>
R=255&middot;N<sub>r</sub><sup><span style="display:none">^(</span>1/2.2<span style="display:none">)</span></sup>,
G=255&middot;N<sub>g</sub><sup><span style="display:none">^(</span>1/2.2<span style="display:none">)</span></sup>,
B=255&middot;N<sub>b</sub><sup><span style="display:none">^(</span>1/2.2<span style="display:none">)</span></sup>
</code></p>

<h2><a name="changeluminosity" />Changing the Luminosity of a Color</h2>

<p>
If you only wish to change the luminosity of a color (L<sup>#</sup>), then this formula can be used:
</p>

<p><code>
L = f<sub>r</sub>&middot;N<sub>r</sub> + f<sub>g</sub>&middot;N<sub>g</sub> + f<sub>b</sub>&middot;N<sub>b</sub>
<br/>m = f<sub>1</sub> + f<sub>0</sub> &middot; (N<sub>0</sub>-N<sub>-1</sub>) / (N<sub>1</sub>-N<sub>-1</sub>)
<br/>D = min { L / m , (1-L) / (1-m) }
<br/>D<sup>#</sup> = min { L<sup>#</sup> / m , (1-L<sup>#</sup>) / (1-m) }
</code></p>

<p><code>
N<sup>#</sup><sub>r</sub> = L<sup>#</sup> + (N<sub>r</sub>-L)&middot;D<sup>#</sup>/D
<br/>N<sup>#</sup><sub>g</sub> = L<sup>#</sup> + (N<sub>g</sub>-L)&middot;D<sup>#</sup>/D
<br/>N<sup>#</sup><sub>b</sub> = L<sup>#</sup> + (N<sub>b</sub>-L)&middot;D<sup>#</sup>/D
</code></p>

<p>
The conversion of R, G and B to N<sub>r, b, g, 1, 0, -1</sub> and
f<sub>r, b, g, 1, 0</sub>, and back from N<sup>#</sup><sub>r, g, b</sub>
to R<sup>#</sup>, B<sup>#</sup> and G<sup>#</sup> is done as described above.
</p>

<h2><a name="changesaturation" />Changing the Saturation of a Color</h2>

<p>
If you only wish to change the saturation of a color (S<sup>#</sup>), then this formula can be used:
</p>

<p><code>
L = f<sub>r</sub>&middot;N<sub>r</sub> + f<sub>g</sub>&middot;N<sub>g</sub> + f<sub>b</sub>&middot;N<sub>b</sub>
<br/>h = (N<sub>0</sub>-N<sub>-1</sub>) / (N<sub>1</sub>-N<sub>-1</sub>)
<br/>m = f<sub>1</sub> + f<sub>0</sub> &middot; h
<br/>D = min { L / m , (1-L) / (1-m) }
</code></p>

<p><code>
N<sup>#</sup><sub>1</sub> = L + S<sup>#</sup>&middot;D&middot;(1-m)
<br/>N<sup>#</sup><sub>0</sub> = L + S<sup>#</sup>&middot;D&middot;(h-m)
<br/>N<sup>#</sup><sub>-1</sub> = L + S<sup>#</sup>&middot;D&middot;m
</code></p>

<p>
The conversion of R, G and B to N<sub>r, b, g, 1, 0, -1</sub> and
f<sub>r, b, g, 1, 0</sub>, and back from N<sup>#</sup><sub>1, 0, -1</sub>
to R<sup>#</sup>, B<sup>#</sup> and G<sup>#</sup> is done as described above.
</p>

<h2><a name="changehue" />Changing the Hue of a Color</h2>

<p>
If you only wish to change the hue of a color (H<sup>#</sup>), then this formula can be used:
</p>

<p><code>
L = f<sub>r</sub>&middot;N<sub>r</sub> + f<sub>g</sub>&middot;N<sub>g</sub> + f<sub>b</sub>&middot;N<sub>b</sub>
<br/>m = f<sub>1</sub> + f<sub>0</sub> &middot; (N<sub>0</sub>-N<sub>-1</sub>) / (N<sub>1</sub>-N<sub>-1</sub>)
<br/>D = min { L / m , (1-L) / (1-m) }
</code></p>

<p><code>
h<sup>#</sup> = 1 - |(H<sup>#</sup> mod 120)/60 - 1|
<br/>m<sup>#</sup> = f<sup>#</sup><sub>1</sub> + f<sup>#</sup><sub>0</sub> &middot; h<sup>#</sup>
<br/>D<sup>#</sup> = min { L / m<sup>#</sup> , (1-L) / (1-m<sup>#</sup>) }
</code></p>

<p><code>
N<sup>#</sup><sub>1</sub> = L + (1-m<sup>#</sup>)&middot;(N<sub>1</sub>-N<sub>-1</sub>)&middot;D<sup>#</sup>/D
<br/>N<sup>#</sup><sub>0</sub> = L + (h<sup>#</sup>-m<sup>#</sup>)&middot;(N<sub>1</sub>-N<sub>-1</sub>)&middot;D<sup>#</sup>/D
<br/>N<sup>#</sup><sub>-1</sub> = L + m<sup>#</sup>&middot;(N<sub>1</sub>-N<sub>-1</sub>)&middot;D<sup>#</sup>/D
</code></p>

<p>
The conversion of R, G, B and H<sup>#</sup> to N<sub>r, b, g, 1, 0, -1</sub>,
f<sub>r, b, g, 1, 0</sub> and f<sup>#</sup><sub>1, 0</sub>, and back from
N<sup>#</sup><sub>1, 0, -1</sub> to R<sup>#</sup>, B<sup>#</sup> and
G<sup>#</sup> is done as described above.
</p>

<p>
(Olaf Schmidt, Gunnar Schmidt)
</p>

<?php
include "footer.inc";
?>
