<?php
  $page_title = "Glossary of accessibility related expressions";
  $site_root = "../";
  include "accessibility.inc";
  include "header.inc";
?>

<style type="text/css"><!--
.explanation {padding-left:2em;}
//--></style>

<p>
 This glossary contains explanations for words that are often used in the
 context of accessibility architectures.
</p>

<div id="quicklinks"> [
  <a href="#a">A</a> |
  <a href="#d">D</a> |
  <a href="#f">F</a> |
  <a href="#g">G</a> |
  <a href="#j">J</a> |
  <a href="#k">K</a> |
  <a href="#m">M</a> |
  <a href="#p">P</a> |
  <a href="#r">R</a> |
  <a href="#s">S</a> |
  <a href="#u">U</a> |
  <a href="#w">W</a> |
  <a href="#x">X</a> ]
</div>

<h2 id="a">A</h2>

<div class="explanation">
<h3 id="a11y">a11y</h3>
 <p>
  Abbreviation for
  <a href="#accessibility"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />Accessibility</a>.
 </p>
</div>

<div class="explanation">
<h3 id="accessibility">Accessibility</h3>
 <p>
  A computer, an application or a web site is fully accessible if it can be
  used by all kind of different devices, including braille devices, screen
  readers, head point devices, special keyboards, buttons or mice, etc.
  The term accessibility is also more generally used to describe projects
  aiming at making computers fully accessible, or at offering computer
  software as a tool for dealing with physical handicaps.
 </p>
</div>

<div class="explanation">
<h3 id="accessibilityaid">Accessibility Aid</h3>
 <p>
  An accessibility aid is a feature, an application or a device that assists
  the user with dealing with a handicap. The term accessibility aid can be
  used as a synonym for
  <a href="#assistivetechnology"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />Assistive Technology</a>.
 </p>
</div>

<div class="explanation">
<h3 id="accessibilitybroker">Accessibility Broker</h3>
 <p>
  Please look at
  <a href="#atspi"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />AT-SPI</a>.
 </p>
</div>

<div class="explanation">
<h3 id="assistivetechnology">Assistive Technology</h3>
 <p>
  An assistive technology is a technology that assists the user with
  dealing with a handicap. In the context of a computer this often is
  an application, but it might also be a braille display, etc.
 </p>
</div>

<div class="explanation">
<h3 id="at">AT</h3>
 <p>
  <a href="#assistivetechnology"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />Assistive Technology</a>.
 </p>
</div>

<div class="explanation">
<h3 id="atag">ATAG</h3>
 <p>
  Authoring Tool Accessibility Guidelines, released by the
  <a href="#wai"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />WAI</a>
  as guidelines for the implementation of accessible web authoring tools.
 </p><p>
  Related internet site:
  <a href="http://www.w3.org/TR/ATAG10/">Authoring Tool Accessibility Guidelines</a>
 </p>
</div>

<div class="explanation">
<h3 id="atk">ATK</h3>
 <p>
  Accessibility Toolkit. This is an interface for accessibility-relevant
  information. It is used in order to devide the toolkit dependent part of
  the accessibility implementation within the Gnome Accessibility Architecture
  from the IPC-dependent part.
 </p>
</div>

<div class="explanation">
<h3 id="atspi">AT-SPI</h3>
 <p>
  Assistive Technology Service Provider Interface. This is a Corba based
  protocol for communication between accessible applications and
  <a href="#assistivetechnology"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />assistive technologies</a>.
  It was designed by the
  <a href="#gap"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />GAP</a>,
  and it is likely to become the
  standard accessibility protocol on Unix systems. AT-SPI is designed to
  know about three different program types: AT-SPI aware applications,
  accessibility clients, and an accessibility broker. The AT-SPI aware
  applications register with the broker in order to offer their information.
  Clients may add event listeners to the broker, so that they get informed
  when accessibility related information in any application changes. Because
  of the relation to the AT-SPI protocol the accessibility broker is often
  called AT-SPI registry.
 </p>
</div>

<h2 id="d">D</h2>

<div class="explanation">
<h3 id="dasher">Dasher</h3>
 <p>
  Dasher is a spatial oriented text input system that uses only a
  pointing-device (like a mouse or an eye tracker, for example) as an
  input method. In order to do so, the characters are displayed within
  boxes (one box for each character). By navigating into one box you
  select the corresponding character. Within that box other boxes are
  added for the second character and so forth.
 </p><p>
  Related internet site:
  <a href="http://www.inference.phy.cam.ac.uk/dasher/">Dasher home page</a>
 </p>
</div>

<h2 id="f">F</h2>

<div class="explanation">
<h3 id="fdawg">FDAWG</h3>
 <p>
  Free Desktop Accessibility Working Group. A mailing list for the discussion
  of the accessibility of free desktops.
 </p><p>
  Related internet sites:
  <a href="http://www.speechinfo.org/fdawg/">FDAWG home page</a>
 </p>
</div>

<h2 id="g">G</h2>

<div class="explanation">
<h3 id="gail">GAIL</h3>
 <p>
  Gnome Accessibility Implementation Library. This library implements a
  bridge between the Gnome widgets and
  <a href="#atk"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />ATK</a>.
 </p>
</div>

<div class="explanation">
<h3 id="gap">GAP</h3>
 <p>
  GNOME Accessibility Project or GNU Accessibility Project.
 </p><p>
  Related internet site:
  <a href="http://developer.gnome.org/projects/gap/">GAP home page</a>
 </p>
</div>

<div class="explanation">
<h3 id="gnopernicus">Gnopernicus</h3>
 <p>
  Gnopernicus is both an assistive technology for low-visioned and blind
  persons. It incorporates supports screen readers, braille displays and
  magnification as its output devices. It uses the
  <a href="#atspi"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />AT-SPI</a>
  protocol for enquiring the information on the screen.
 </p><p>
  Related internet sites: <a href="http://www.baum.ro/gnopernicus.html">Gnopernicus home page</a>
 </p>
</div>

<div class="explanation">
<h3 id="gok">GOK</h3>
 <p>
  Gnome Onscreen Keyboard. It is an application that displays a dynamically
  changing keyboard on the screen and allows multiple input methods for
  selection. GOK uses the
  <a href="#atspi"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />AT-SPI</a>
  protocol for updating the keys within the keyboard.
 </p><p>
  Related internet sites: <a href="http://www.gok.ca/">GOK home page</a>
 </p>
</div>

<h2 id="j">J</h2>

<div class="explanation">
<h3 id="jsapi">JSAPI</h3>
 <p>
  Java Speech Application Programming Interface. The JSAPI is a cross-platform
  programming interface to support command and control recognizers, dictation
  systems and speech synthesizers. 
 </p><p>
  Related internet site:
  <a href="http://java.sun.com/products/java-media/speech/">JSAPI home page</a>
 </p>
</div>

<h2 id="k">K</h2>

<div class="explanation">
<h3 id="kdeaccessibility">kdeaccessibility</h3>
 <p>
  kdeaccessibility is a package of
  <a href="#assistivetechnology"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />assistive technologies</a>
  for KDE. It is developed by the
  <a href="#kdeap"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />KDEAP</a>.
  The kdeaccessibility package currently contains 
  <a href="#kmagnifier"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />KMagnifier</a>,
  <a href="#kmousetool"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />KMouseTool</a>, and
  <a href="#kmouth"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />KMouth</a>.
 </p><p>
  Related internet site:
  <a href="http://accessibility.kde.org/aids/">Accessibility Aids for KDE</a>
 </p>
</div>

<div class="explanation">
<h3 id="kdeap">KDEAP</h3>
 <p>
  KDE Accessibility Project.
 </p><p>
  Related internet site:
  <a href="http://accessibility.kde.org/">KDEAP home page</a>
 </p>
</div>

<div class="explanation">
<h3 id="kmag">kmag</h3>
 <p>
  Binary name of
  <a href="#kmagnifier"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />KMagnifier</a>.
 </p>
</div>

<div class="explanation">
<h3 id="kmagnifier">KMagnifier</h3>
 <p>
  KMagnifier is a screen magnifier. It magnifies the area of the screen around the
  mouse pointer or optionally a user defined area. Additionally it offers to save
  a magnified screenshot to disk. It is part of the
  <a href="#kdeaccessibility"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />kdeaccessibility</a>
  package, but also has its own homepage. KMagnifier is often called kmag.
 </p><p>
  Related internet site:
  <a href="http://kmag.sourceforge.net/">KMagnifier home page</a>
 </p>
</div>

<div class="explanation">
<h3 id="kmousetool">KMouseTool</h3>
 <p>
  KMouseTool is a program that clicks whenever the user stops moving the
  mouse. This is needed by persons whom it hurts clicking with one of the
  mouse keys. KMouseTool is part of the
  <a href="#kdeaccessibility"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />kdeaccessibility</a>
  package, but also has its own hompage.
 </p><p>
  Related internet site:
  <a href="http://www.mousetool.com/">KMouseTool home page</a>
 </p>
</div>

<div class="explanation">
<h3 id="kmouth">KMouth</h3>
 <p>
  KMouth is a program for people who cannot speak. It has a text input field
  and speaks the sentences that you enter. It also has support for user defined
  phrasebooks. KMouth is part of the
  <a href="#kdeaccessibility"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />kdeaccessibility</a>
  package, but also has its own hompage.
 </p><p>
  Related internet site:
  <a href="http://www.schmi-dt.de/kmouth/">KMouth home page</a>
 </p>
</div>

<div class="explanation">
<h3 id="kttsd">kttsd</h3>
 <p>
  KDE Text-To-Speech Deamon. This is a project that aims to give KDE
  applications a standardized way for speech synthesis. In order to do so,
  the driver for the actual speech synthesizer is loaded as a plug-in.
  kttsd is yet in development, but will eventually be added either to
  the kdelibs, kdebase or
  <a href="#kdeaccessibility"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />kdeaccessibility</a>
  package.
 </p>
</div>

<h2 id="m">M</h2>

<div class="explanation">
<h3 id="msaa">MSAA</h3>
 <p>
  Microsoft Active Accessibility. This is a part of the Microsoft Windows
  operating system that collects accessibility relevant information from
  MSAA aware applications and offers these information to
  <a href="#assistivetechnology"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />assistive technologies</a>.
  MSAA was not designed to be the only source of information that is used by assistive
  technologies. It rather complements existing technologies for determining
  what is on the screen (such as
  <a href="#msam"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />MSAM</a>).
 </p>
</div>

<div class="explanation">
<h3 id="msam">MSAM</h3>
 <p>
  Microsoft Screen Access Model. This is a technology that can be used on the
  Microsoft Windows operating system for enquiring screen contents from any
  application. MSAM does only provide screen contents, not meta-information
  (as the
  <a href="#role"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />role</a> or the
  <a href="#state"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />state</a> of a GUI element).
  In order to get access to these meta-information
  <a href="#assistivetechnology"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />assistive technologies</a> have to use
  <a href="#msam"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />MSAA</a>
    in addition to MSAM.
 </p>
</div>

<h2 id="p">P</h2>

<div class="explanation">
<h3 id="proklam">Proklam</h3>
 <p>
  Former name of
  <a href="#kttsd"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />kttsd</a>
 </p>
</div>

<h2 id="r">R</h2>

<div class="explanation">
<h3 id="relation">Relation</h3>
 <p>
  The relation between two GUI elements. Examples for such relations are that
  one element is a label for an other element (for example a label for a
  text input field), an element is controlled by an other element or that
  the contents logically flow from one element to an other.
 </p>
</div>

<div class="explanation">
<h3 id="role">Role</h3>
 <p>
  The role of a GUI element. An element can be an alert to the user, an
  arrow, an object that allows to select a date, a check box, a dialog,
  a menu etc.
 </p>
</div>

<h2 id="s">S</h2>

<div class="explanation">
<h3 id="speakerplugins">Speaker plug-ins</h3>
 <p>
  The Speaker plug-ins add a very simple text-to-speech function to Konqueror and Kate.
  Older versions call IBM ViaVoice or Festival directly; a new version will be released with
  <a href="#kttsd"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />kttsd</a>.
  The Speaker plug-ins are very simple and do not have the functionality of a screen reader like
  <a href="#gnopernicus"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />Gnopernicus</a>.
 </p>
</div>

<div class="explanation">
<h3 id="state">State</h3>
 <p>
  The state of a GUI element. For example an element can be active,
  editable, focusable, focused, selectable, selected etc.
 </p>
</div>

<h2 id="u">U</h2>

<div class="explanation">
<h3 id="uaa">UAA</h3>
 <p>
  <a href="#unoaccessibilityapi"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />UNO Accessibility API</a>
 </p>
</div>

<div class="explanation">
<h3 id="uaag">UAAG</h3>
 <p>
  User Agent Accessibility Guidelines, released by the
  <a href="#wai"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />WAI</a>
  as guidelines for the implementation of accessible internet browsers.
 </p><p>
  Related internet site:
  <a href="http://www.w3.org/TR/UAAG10/">User Agent Accessibility Guidelines</a>
 </p>
</div>

<div class="explanation">
<h3 id="unoaccessibilityapi">UNO Accessibility API</h3>
 <p>
  Universal Network Objects Accessibility Application Programming Interface.
  This is the accessibility framework of OpenOffice.org.
 </p><p>
  Related internet sites:
  <a href="http://ui.openoffice.org/accessibility/">OpenOffice.org Accessibility Project</a>,
  <a href="http://udk.openoffice.org/common/man/uno.html">Universal Network Objects (UNO)</a>,
  <a href="http://ui.openoffice.org/accessibility/unoapi.html">UNO Accessibility API (UAA)</a>
 </p>
</div>

<h2 id="w">W</h2>

<div class="explanation">
<h3 id="wai">WAI</h3>
 <p>
  Web Accessibility Initiative, a sub-project of the World Wide Web Consortium
  (W3C) pursuing accessibility of the Web.
 </p><p>
  Related internet sites:
  <a href="http://www.w3.org/WAI/">WAI home page</a>,
  <a href="http://www.w3.org">W3C home page</a>
 </p>
 </div>

<div class="explanation">
 <h3 id="wcag">WCAG</h3>
  <p>
  Web Content Accessibility Guidelines, released by the
  <a href="#wai"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />WAI</a>
  as guidelines for the creation of accessible sites.
 </p><p>
  Related internet site:
  <a href="http://www.w3.org/TR/WCAG10/">Web Content Accessibility Guidelines</a>
 </p>
</div>

<h2 id="x">X</h2>

<div class="explanation">
<h3 id="xag">XAG</h3>
 <p>
  XML Accessibility Guidelines, written by the
  <a href="#wai"><img src="images/lookat.gif" width="14" height="10" border="0"
    alt="" />WAI</a>
  as guidelines for the implementation of accessible XML based-applications.
 </p><p>
  Related internet site:
  <a href="http://www.w3.org/TR/xag.html">XML Accessibility Guidelines</a>
 </p>
</div>

<?php
  include_once ("footer.inc");
?>
