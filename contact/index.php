<?php
  $page_title = "Contact";
  $site_root = "../";
  include "accessibility.inc";
  include "header.inc";
?>

<p>If you wish to contact the KDE Accessibility Project, please post a message
  to the kde-accessibility mailing list
  (<a href="http://lists.kde.org/?l=kde-accessibility">Archive</a>, <a href="http://mail.kde.org/mailman/listinfo/kde-accessibility">subscription info</a>).
  If you have any ideas, suggestions, questions, you are very welcome.
</p>

<div id="quicklinks"> [
  <a href="#tasks">Tasks within the KDEAP</a> |
  <a href="#contributors">Contributors to the KDEAP</a> ]
</div>

<h2 id="tasks">Tasks within the KDEAP</h2>
  <ul>
   <li>The kdeaccessibility package and the interoperability efforts are maintained by Gunnar Schmi Dt
    (<a href="mailto:gunnar at schmi-dt dot de">E-mail to Gunnar</a>).</li>
   <li>The Qt-ATK bridge is maintained by Harald Fernengel
    (<a href="mailto:harald at trolltech dot com">E-mail to Harald</a>).</li>
   <li>This web site is maintained by Olaf Jan Schmidt
    (<a href="mailto:ojschmidt at kde dot org">E-mail to Olaf</a>).</li>
  </ul>

<h2 id="contributors">Contributors to the KDEAP</h2>
<p>
  Contributors to the KDEAP are:
</p>
  <ul>
   <li>Jeremy Whiting
    <ul>
     <li>Current maintainer of the KDE text to speech daemon kttsd.</li>
     <li><a href="mailto:jpwhiting at kde dot org">E-mail to Jeremy</a> - <a href="http://jpwhiting.blogspot.com">Jeremy's blog</a></li>
    </ul>
   </li>
   <li>Gary Cramblitt
    <ul>
     <li>Previous maintainer of the KDE text to speech deamon kttsd.</li>
     <li><a href="mailto:garycramblitt at comcast dot net">E-mail to Gary</a></li>
    </ul>
   </li>
   <li>Jos&eacute; Pablo Ezequiel Fern&aacute;ndez (&quot;Pupeno&quot;)
    <ul>
     <li>Original author of kttsd</li>
     <li><a href="mailto:pupeno at kde dot org">E-mail to Pupeno</a> - <a href="http://www.pupeno.com/">Pupeno's web site</a></li>
    </ul>
   </li>
   <li>Harald Fernengel
    <ul>
     <li>Author of the Qt-ATK bridge</li>
     <li><a href="mailto:harald at trolltech dot com">E-mail to Harald</a></li>
    </ul>
   </li>
   <li>Sarang Lakare
    <ul>
     <li>Maintainer of KMagnifier (kmag)</li>
     <li><a href="mailto:sarang at users dot sourceforge dot net">E-Mail to Sarang</a> - <a href="http://www.linkedin.com/in/saranglakare">Sarang's web site</a></li>
    </ul>
   </li>
   <li>Jeff Roush
    <ul>
     <li>Maintainer of KMouseTool</li>
     <li><a href="mailto:jeff at mousetool dot com">E-mail to Jeff</a> - <a href="http://www.mousetool.com/repetitive-strain-injury.html">Jeff's web site</a></li>
    </ul>
   </li>
   <li>George Russell
    <ul>
     <li>Original author of the Speaker plug-ins for Konqueror and Kate</li>
     <li><a href="mailto:george dot russel at clara dot net">E-mail to George</a> - <a href="http://dogma.freebsd-uk.eu.org/~grrussel/">George's web site</a></li>
    </ul>
   </li>
   <li>Gunnar Schmi Dt
    <ul>
     <li>Maintainer of the kdeaccessibility CVS module, coordination of development effords</li>
     <li>Coordination of accessibility related interoperability effords</li>
     <li>Maintainer of KMouth, maintainer of the "Hadifax" and "Command" kttsd plug-ins</li>
     <li><a href="mailto:gunnar at schmi-dt dot de">E-mail to Gunnar</a> - <a href="http://www.schmi-dt.de">Gunnar's web site</a></li>
    </ul>
   </li>
   <li>Olaf Jan Schmidt
    <ul>
     <li>Maintainer of the <a href="http://accessibility.kde.org">KDEAP web site</a></li>
     <li>Promotion and PR (stories for <a href="http://dot.kde.org">KDE News</a>, etc.), educating the KDE community about accessibility issues, participation in general kde.org webmaster work to ensure better accessibility</li>
     <li>Maintainer of the Speaker plug-ins</li>
     <li>Reporting and sometimes fixing bugs</li>
     <li><a href="mailto:ojschmidt at kde dot org">E-mail to Olaf</a></li>
    </ul>
   </li>
   <li>JP Schnapper-Casteras
    <ul>
     <li>Founder of the KDEAP</li>
     <li><a href="mailto:jpsc at stanfordalumni dot org">E-mail to JP</a></li>
    </ul>
   </li>
  </ul>
<p>
  <a href="&#109;ailt&#111;:w&#0101;&#x62;ma&#115;&#00116;&#x65;&#x72;&#x40;ac&#99;&#x65;&#x73;s&#x69;bil&#105;&#00116;y kd&#0101;&#0046;o&#x72;&#x67;">E-mail to the KDEAP webmaster</a>
</p>

<?php
  include_once ("footer.inc");
?>
