<?php
  $page_title = "Accessibility Aids For KDE";
  $site_root = "../";
  include "accessibility.inc";
  include "header.inc";
?>

<p>
 The kdeaccessibility package contains various KDE Accessibility Aids: KMouseTool, KMag, KMouth and Jovie (formerly KTTS).
</p>
 
<h2>Information has moved to userbase</h2>

<h2 id="kmousetool"><a href="http://userbase.kde.org/KMouseTool">KMouseTool</a></h2>
 
<h2 id="kmag"><a href="http://userbase.kde.org/KMag/">KMagnifier</a></h2>

<h2 id="kmouth"><a href="http://userbase.kde.org/KMouth/">KMouth</a></h2>
 
<h2><a href="http://userbase.kde.org/Jovie/">Jovie</a></h2>

<?php
  include_once ("footer.inc");
?>

