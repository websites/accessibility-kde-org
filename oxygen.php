<?php
$page_title = "Contrast Evalutation";
$site_root = "./";
include "accessibility.inc";
include "header.inc";

function compareHex ($a, $b) {
  return $a ["hex"] > $b ["hex"] ? 1 : ($a ["hex"] < $b ["hex"] ? -1 : 0);
};
function compareChroma ($a, $b) {
  if ($a ["chroma"] > $b ["chroma"])
    return -1;
  elseif ($a ["chroma"] < $b ["chroma"])
    return 1;
  else
    return compareHue ($a, $b);
};
function compareSaturation ($a, $b) {
  if ($a ["saturation"] > $b ["saturation"])
    return -1;
  elseif ($a ["saturation"] < $b ["saturation"])
    return 1;
  else
    return compareHue ($a, $b);
};
function compareHue ($a, $b) {
  if (is_float ($a ["hue"]) && ! is_float ($b ["hue"]))
    return -1;
  elseif (! is_float ($a ["hue"]) && is_float ($b ["hue"]))
    return 1;
  elseif  (! is_float ($a ["hue"]) && ! is_float ($b ["hue"]))
    return compareLuminosity ($a, $b);
  elseif ($a ["hue"] > $b ["hue"])
    return 1;
  elseif ($a ["hue"] < $b ["hue"])
    return -1;
  else
    return compareLuminosity ($a, $b);
};
function compareLuminosity ($a, $b) {
  if ($a ["luminosity"] > $b ["luminosity"])
    return -1;
  elseif ($a ["luminosity"] < $b ["luminosity"])
    return 1;
  else
    return 0;
};
function compareContrast ($a, $b)
{ global $extracolors;

  $extracolor = reset ($extracolors);
  $contrastA = luminosityContrast ($a, $extracolor);
  $contrastB = luminosityContrast ($b, $extracolor);

  if ($contrastA > $contrastB)
    return -1;
  elseif ($contrastA < $contrastB)
    return 1;
  else
    return 0;  
}

function luminosityContrast (const $color1, const  $color2) {
  return (max ($color1 ["luminosity"], $color2 ["luminosity"]) + 0.05) / (min ($color1 ["luminosity"], $color2 ["luminosity"]) + 0.05);
}
function brightnessContrast (const $color1, const $color2) {
  return abs ($color1 ["brightness"] - $color2 ["brightness"]);
}
function colorContrast (const $color1, const $color2) {
  return abs ($color1 ["red256"] - $color2 ["red256"]) + abs ($color1 ["green256"] - $color2 ["green256"]) + abs ($color1 ["blue256"] - $color2 ["blue256"]);
}
function hasContrastWCAG1 (const $color1, const $color2) {
  return (brightnessContrast ($color1, $color2) >= 125) && (colorContrast ($color1, $color2) >= 500);
}

function evaluateColor (const $colors, const $color, const $name, const $tablename) {
  $contrastGood=0;  $contrastSufficient=0;
  foreach ($colors as $oxygencolor) {
    if (luminosityContrast ($color, $oxygencolor) >= 5)
      $contrastSufficient++;
  }

  echo '<div>"<a href="?color=' . urlencode ($name) . '">' . $name . '</a>"';
  echo ' contrasts sufficiently with ' . $contrastSufficient . ' ' . $tablename . ' colors.</div>';
}
function computeColorFromRGB (&$color) {
    $color ["redL"] = round (pow ($color ["red256"]/255, 2.2), 10);
    $color ["greenL"] = round (pow ($color ["green256"]/255, 2.2), 10);
    $color ["blueL"] = round (pow ($color ["blue256"]/255, 2.2), 10);
    $color ["hex"] = sprintf ("#%'02X%'02X%'02X", $color ["red256"], $color ["green256"], $color ["blue256"]);

    $color ["luminosity"] = 0.2126 * $color ["redL"] + 0.7152 * $color ["greenL"] + 0.0722 * $color ["blueL"];
    $color ["brightness"] = (299 * $color ["red256"] + 587 * $color ["green256"] + 114 * $color ["blue256"]) / 1000;

    $max = max ($color ["redL"], $color ["greenL"], $color ["blueL"]);
    $min = min ($color ["redL"], $color ["greenL"], $color ["blueL"]);

    if ($max == $min) {
       $color ["hue"] = "-";
       $color ["saturation"] = 0;
       $color ["chroma"] = 0;
    }
    else {
      $med = $color ["redL"] + $color ["greenL"] + $color ["blueL"] - $max - $min;
      $hue = ($med - $min) / ($max - $min);

      if ($max == $color ["redL"]) {
        if ($min == $color ["blueL"]) {
          $color ["hue"] = round ($hue/6, 10);
        } else {
          $color ["hue"] = round (1 - $hue/6, 10);
        }
      } elseif ($max == $color ["greenL"]) {
        if ($min == $color ["blueL"]) {
          $color ["hue"] = round (1/3 - $hue/6, 10);
        } else {
          $color ["hue"] = round (1/3 + $hue/6, 10);
        }
      } else {
        if ($min == $color ["greenL"]) {
          $color ["hue"] = round (2/3 + $hue/6, 10);
        } else {
          $color ["hue"] = round (2/3 - $hue/6, 10);
        }
      }

      $color ["chroma"] = round (max (($max-$min)*0.0722/$color ["luminosity"], ($max-$min)*0.0722/(1-$color ["luminosity"])), 10);
      $color ["saturation"] = round (max (($color ["luminosity"]-$min)/$color ["luminosity"], ($max-$color ["luminosity"])/(1-$color ["luminosity"])), 10);
    }
}

function computeColorFromHSL (&$color) {
    // ported from Matthew's implementation in kdelibs
    // calculate some needed variables
    $hs = $color ["hue"] * 6.0;
    if ($hs < 1.0) {
        $th = $hs;
        $tm = 0.2126 + 0.7152 * $th;
    }
    else if ($hs < 2.0) {
        $th = 2.0 - $hs;
        $tm = 0.7152 + 0.2126 * $th;
    }
    else if ($hs < 3.0) {
        $th = $hs - 2.0;
        $tm = 0.7152 + 0.0722 * $th;
    }
    else if ($hs < 4.0) {
        $th = 4.0 - $hs;
        $tm = 0.0722 + 0.7152 * $th;
    }
    else if ($hs < 5.0) {
        $th = $hs - 4.0;
        $tm = 0.0722 + 0.2126 * $th;
    }
    else {
        $th = 6.0 - $hs;
        $tm = 0.2126 + 0.0722 * $th;
    }

    // calculate RGB channels in sorted order
    if ($tm >= $color ["luminosity"]) {
        $tp = $color ["luminosity"] + $color ["luminosity"] * $color ["saturation"] * (1.0 - $tm) / $tm;
        $to = $color ["luminosity"] + $color ["luminosity"] * $color ["saturation"] * ($th - $tm) / $tm;
        $tn = $color ["luminosity"] - ($color ["luminosity"] * $color ["saturation"]);
    }
    else {
        $tp = $color ["luminosity"] + (1.0 - $color ["luminosity"]) * $color ["saturation"];
        $to = $color ["luminosity"] + (1.0 - $color ["luminosity"]) * $color ["saturation"] * ($th - $tm) / (1.0 - $tm);
        $tn = $color ["luminosity"] - (1.0 - $color ["luminosity"]) * $color ["saturation"] * $tm / (1.0 - $tm);
    }

    // return RGB channels in appropriate order
    if ($hs < 1.0) {
        $color ["redL"] = $tp; $color ["greenL"] = $to; $color ["blueL"] = $tn;
    } else if ($hs < 2.0) {
        $color ["redL"] = $to; $color ["greenL"] = $tp; $color ["blueL"] = $tn;
    } else if ($hs < 3.0) {
        $color ["redL"] = $tn; $color ["greenL"] = $tp; $color ["blueL"] = $to;
    } else if ($hs < 4.0) {
        $color ["redL"] = $tn; $color ["greenL"] = $to; $color ["blueL"] = $tp;
    } else if ($hs < 5.0) {
        $color ["redL"] = $to; $color ["greenL"] = $tn; $color ["blueL"] = $tp;
    } else {
        $color ["redL"] = $tp; $color ["greenL"] = $tn; $color ["blueL"] = $to;
    }

    $color ["red256"] = round (pow ($color ["redL"], 1/2.2)*255);
    $color ["green256"] = round (pow ($color ["greenL"], 1/2.2)*255);
    $color ["blue256"] = round (pow ($color ["blueL"], 1/2.2)*255);
    $color ["hex"] = sprintf ("#%'02X%'02X%'02X", $color ["red256"], $color ["green256"], $color ["blue256"]);

    $color ["brightness"] = (299 * $color ["red256"] + 587 * $color ["green256"] + 114 * $color ["blue256"]) / 1000;

    $max = max ($color ["redL"], $color ["greenL"], $color ["blueL"]);
    $min = min ($color ["redL"], $color ["greenL"], $color ["blueL"]);
    if ($max == $min)
        $color ["chroma"] = 0;
    else
        $color ["chroma"] = round (max (($max-$min)*0.0722/$color ["luminosity"], ($max-$min)*0.0722/(1-$color ["luminosity"])), 10);

}

function tint1 (const $basecolor, const $tintcolor, $amount) {
    // ported from Matthew's implementation in kdelibs
    $newcolor = array ();

    $newcolor ["red256"] = round ($basecolor ["red256"] + pow ($amount, 0.5) * ($tintcolor ["red256"] - $basecolor ["red256"]));
    $newcolor ["green256"] = round ($basecolor ["green256"] + pow ($amount, 0.5) * ($tintcolor ["green256"] - $basecolor ["green256"]));
    $newcolor ["blue256"] = round ($basecolor ["blue256"] + pow ($amount, 0.5) * ($tintcolor ["blue256"] - $basecolor ["blue256"]));
    computeColorFromRGB ($newcolor);

    $newcolor ["luminosity"] = $basecolor ["luminosity"] + $amount * ($newcolor ["luminosity"] - $basecolor ["luminosity"]);
    computeColorFromHSL ($newcolor);

    return $newcolor;
}

function tint2 (const $basecolor, const $tintcolor, $amount) {
    $newcolor = array ();

    $newcolor ["hue"] = $tintcolor ["hue"];
    $newcolor ["saturation"] = $tintcolor ["saturation"];
    if ($basecolor ["luminosity"] < 0.07)
        $newcolor ["luminosity"] = min ($tintcolor ["luminosity"], 0.07);
    else if ($basecolor ["luminosity"] > 0.65)
        $newcolor ["luminosity"] = max ($tintcolor ["luminosity"], 0.65);
    else $newcolor ["luminosity"] = $basecolor ["luminosity"];
    computeColorFromHSL ($newcolor);

    if ($amount < 0.5) {
        $newcolor ["red256"] += round ((1-2*$amount) * ($basecolor ["red256"] - $newcolor ["red256"]));
        $newcolor ["green256"] += round ((1-2*$amount) * ($basecolor ["green256"] - $newcolor ["green256"]));
        $newcolor ["blue256"] += round ((1-2*$amount) * ($basecolor ["blue256"] - $newcolor ["blue256"]));
    } else {
        $newcolor ["red256"] += round ((2*$amount-1) * ($tintcolor ["red256"] - $newcolor ["red256"]));
        $newcolor ["green256"] += round ((2*$amount-1) * ($tintcolor ["green256"] - $newcolor ["green256"]));
        $newcolor ["blue256"] += round ((2*$amount-1) * ($tintcolor ["blue256"] - $newcolor ["blue256"]));
    }
    computeColorFromRGB ($newcolor);

    return $newcolor;
}

function printTableLine (const $colors, const $color, const $name, const $tablename) {
  global $extracolors;
?>
  <tr style="vertical-align:top;">
  <td style="background-color:<?php echo $color ["hex"]; ?>">&nbsp;&nbsp;&nbsp;&nbsp;</td>
  <td style="text-align:left;">
    <a href="?color=<?php echo urlencode ($name) ?>"><?php echo $name ?>
    <br/>(<?php echo $color ["hex"] ?>)</a>
  </td>
  <td style="text-align:center;">
    <?php if (is_float ($color ["hue"])) printf ("%.0f&deg;", $color ["hue"]*360); else echo $color ["hue"]; ?>
  </td>
  <td style="text-align:center;">
    <?php printf ("%.0f%%", $color ["chroma"]*100); ?>, <?php printf ("%.0f%%", $color ["saturation"]*100) ?>
  </td>
  <td style="text-align:center;"><?php printf ("%.1f%%", $color ["luminosity"]*100) ?></td>
  <td style="text-align:left;">
  <?php
    if (count ($extracolors) == 0)
      evaluateColor ($colors, $color, $name, $tablename);
    else foreach ($extracolors as $extraname=>$extracolor) {
      if ($extracolor ["hex"] != $color ["hex"]) {
        echo '<div>';
        $contrastL = luminosityContrast ($color, $extracolor);

        $contrastC = colorContrast ($color, $extracolor);
        echo '<span style="background-color:'.$color ["hex"].';color:'.$extracolor ["hex"].'">';
        echo $contrastL >= 5 ? "[O." : "[K.";
        echo '</span>';
        echo '<span style="background-color:'.$extracolor ["hex"].';color:'.$color ["hex"].'">';
        echo $contrastL >= 5 ? "K.]" : "O.]";
        echo '</span>&nbsp;';
        echo $contrastL < 5 ? '<b>Insufficient contrast</b>' : ($contrastL >= 10 ? "<em>Good contrast</em>" : "Sufficient contrast");

        printf (' (%.1f) with "%s"', $contrastL, $extraname);
        if (hasContrastWCAG1 ($color, $extracolor))
          printf (' (WCAG1: pass)', $contrastB);
        else
          printf (' (WCAG1: fail)', $contrastC);
        echo '</div>';
        echo '<div>';
        echo 'Tint1: ';
        for ($amount = 0; $amount <= 10; $amount++) {
            echo '<span style="background-color:';
            $newcolor = tint1 ($extracolor, $color, $amount/10);
            echo $newcolor ["hex"].';color:';
            echo ($newcolor ["luminosity"] <= 0.09) ? "#FFFFFF" : (($newcolor ["luminosity"] >= 0.55) ? "#000000" : "#800000");
            echo '">';
            printf ('%.1f ', $amount/10);
            echo '</span>';
        }
        echo '</div>';
        echo '<div>';
        echo 'Tint2: ';
        for ($amount = 0; $amount <= 10; $amount++) {
            echo '<span style="background-color:';
            $newcolor = tint2 ($extracolor, $color, $amount/10);
            echo $newcolor ["hex"].';color:';
            echo ($newcolor ["luminosity"] <= 0.09) ? "#FFFFFF" : (($newcolor ["luminosity"] >= 0.55) ? "#000000" : "#800000");
            echo '">';
            printf ('%.1f ', $amount/10);
            echo '</span>';
        }
        echo '</div>';
      }
    }
?>
  </td>
 </tr>
<?php
}

function printTable (const $colors, const $tablename) {
  global $extracolors;
?>
<table><thead><tr>
 <th colspan="2" style="text-align:center;"><a href="?sortby=name">Name</a> <br/>(<a href="?sortby=hex">Hexadecimal</a>)</th>
 <th style="text-align:center;"><a href="?sortby=hue">Hue</a></th>
 <th style="text-align:center;"><a href="?sortby=chroma">Chroma</a>, <a href="?sortby=saturation">Saturation</a></th>
 <th style="text-align:center;"><a href="?sortby=luminosity">Luminosity</a></th>
 <th style="text-align:left;">Luminosity Contrast</th>
</tr></thead>
<tbody>
<?php
  foreach ($colors as $name=>$color)
    printTableLine ($colors, $color, $name, $tablename);
?>
</tbody></table>
<?php
}

$defaultcolors = array (
    "black"=>array ("red256"=>0, "green256"=>0, "blue256"=>0),
    "white"=>array ("red256"=>255, "green256"=>255, "blue256"=>255),
    "selection background"=>array ("red256"=>36, "green256"=>96, "blue256"=>170),
    "window background"=>array ("red256"=>239, "green256"=>239, "blue256"=>239),
    "button background"=>array ("red256"=>221, "green256"=>221, "blue256"=>221),
    "active window"=>array ("red256"=>0, "green256"=>87, "blue256"=>174),
    "inactive window"=>array ("red256"=>85, "green256"=>85, "blue256"=>85),
    "text 2"=>array ("red256"=>0, "green256"=>0, "blue256"=>191),
    "text 3"=>array ("red256"=>90, "green256"=>0, "blue256"=>179),
    "text 4"=>array ("red256"=>80, "green256"=>80, "blue256"=>0),
    "text 5"=>array ("red256"=>176, "green256"=>0, "blue256"=>0),
    "text 6"=>array ("red256"=>0, "green256"=>77, "blue256"=>0),
    "text 7"=>array ("red256"=>51, "green256"=>99, "blue256"=>102),
    "text 8"=>array ("red256"=>85, "green256"=>85, "blue256"=>85),
    "background 2"=>array ("red256"=>223, "green256"=>236, "blue256"=>255),
    "background 3"=>array ("red256"=>239, "green256"=>223, "blue256"=>255),
    "background 4"=>array ("red256"=>251, "green256"=>255, "blue256"=>223),
    "background 5"=>array ("red256"=>249, "green256"=>227, "blue256"=>226),
    "background 6"=>array ("red256"=>234, "green256"=>255, "blue256"=>209),
    "background 7"=>array ("red256"=>223, "green256"=>253, "blue256"=>255),
    "background 8"=>array ("red256"=>247, "green256"=>247, "blue256"=>247),
    "focussed text"=>array ("red256"=>128, "green256"=>63, "blue256"=>0),
    "focussed background"=>array ("red256"=>255, "green256"=>236, "blue256"=>215),
    "focussed selection"=>array ("red256"=>255, "green256"=>235, "blue256"=>85),
    "tool-tip"=>array ("red256"=>255, "green256"=>246, "blue256"=>200)
);

$cigcolors = array (
    "black"=>array ("red256"=>0, "green256"=>0, "blue256"=>0),
    "white"=>array ("red256"=>255, "green256"=>255, "blue256"=>255),
    "CIG brown 1"=>array ("red256"=>82, "green256"=>34, "blue256"=>0),
    "CIG brown 2"=>array ("red256"=>159, "green256"=>95, "blue256"=>0),
    "CIG brown 3"=>array ("red256"=>217, "green256"=>174, "blue256"=>126),
    "CIG brown 4"=>array ("red256"=>243, "green256"=>223, "blue256"=>198),
    "CIG red 1"=>array ("red256"=>181, "green256"=>0, "blue256"=>47),
    "CIG red 2"=>array ("red256"=>231, "green256"=>35, "blue256"=>0),
    "CIG red 3"=>array ("red256"=>240, "green256"=>164, "blue256"=>185),
    "CIG red 4"=>array ("red256"=>246, "green256"=>207, "blue256"=>221),
    "CIG yellow 1"=>array ("red256"=>249, "green256"=>186, "blue256"=>7),
    "CIG yellow 2"=>array ("red256"=>255, "green256"=>220, "blue256"=>0),
    "CIG yellow 3"=>array ("red256"=>255, "green256"=>233, "blue256"=>68),
    "CIG yellow 4"=>array ("red256"=>255, "green256"=>245, "blue256"=>146),
    "CIG green 1"=>array ("red256"=>83, "green256"=>147, "blue256"=>22),
    "CIG green 2"=>array ("red256"=>99, "green256"=>176, "blue256"=>31),
    "CIG green 3"=>array ("red256"=>127, "green256"=>187, "blue256"=>86),
    "CIG green 4"=>array ("red256"=>180, "green256"=>213, "blue256"=>151),
    "CIG green 5"=>array ("red256"=>0, "green256"=>102, "blue256"=>47),
    "CIG green 6"=>array ("red256"=>0, "green256"=>151, "blue256"=>84),
    "CIG green 7"=>array ("red256"=>128, "green256"=>195, "blue256"=>155),
    "CIG green 8"=>array ("red256"=>171, "green256"=>215, "blue256"=>188),
    "CIG blue 1"=>array ("red256"=>0, "green256"=>65, "blue256"=>136),
    "CIG blue 2"=>array ("red256"=>0, "green256"=>113, "blue256"=>188),
    "CIG blue 3"=>array ("red256"=>0, "green256"=>148, "blue256"=>213),
    "CIG blue 4"=>array ("red256"=>179, "green256"=>221, "blue256"=>245),
    "CIG blue 5"=>array ("red256"=>27, "green256"=>45, "blue256"=>131),
    "CIG blue 6"=>array ("red256"=>74, "green256"=>108, "blue256"=>179),
    "CIG blue 7"=>array ("red256"=>95, "green256"=>143, "blue256"=>203),
    "CIG blue 8"=>array ("red256"=>197, "green256"=>217, "blue256"=>240),
    "CIG violet 1"=>array ("red256"=>85, "green256"=>18, "blue256"=>123),
    "CIG violet 2"=>array ("red256"=>121, "green256"=>103, "blue256"=>171),
    "CIG violet 3"=>array ("red256"=>164, "green256"=>158, "blue256"=>205),
    "CIG violet 4"=>array ("red256"=>191, "green256"=>184, "blue256"=>216),
    "CIG violet 5"=>array ("red256"=>123, "green256"=>12, "blue256"=>130),
    "CIG violet 6"=>array ("red256"=>155, "green256"=>87, "blue256"=>159),
    "CIG violet 7"=>array ("red256"=>186, "green256"=>144, "blue256"=>192),
    "CIG violet 8"=>array ("red256"=>209, "green256"=>176, "blue256"=>210),
    "CIG gray 1"=>array ("red256"=>0, "green256"=>0, "blue256"=>28),
    "CIG gray 2"=>array ("red256"=>11, "green256"=>17, "blue256"=>45),
    "CIG gray 3"=>array ("red256"=>68, "green256"=>78, "blue256"=>90),
    "CIG gray 4"=>array ("red256"=>109, "green256"=>113, "blue256"=>121),
    "CIG gray 5"=>array ("red256"=>157, "green256"=>161, "blue256"=>166),
    "CIG gray 6"=>array ("red256"=>187, "green256"=>191, "blue256"=>195),
    "CIG gray 7"=>array ("red256"=>218, "green256"=>221, "blue256"=>224),
    "CIG gray 8"=>array ("red256"=>235, "green256"=>236, "blue256"=>237)
);
$oxygencolors = array (
    "black"=>array ("red256"=>0, "green256"=>0, "blue256"=>0),
    "white"=>array ("red256"=>255, "green256"=>255, "blue256"=>255),
    "wood brown6"=>array ("red256"=>56, "green256"=>37, "blue256"=>9),
    "wood brown5"=>array ("red256"=>87, "green256"=>64, "blue256"=>30),
    "wood brown4"=>array ("red256"=>117, "green256"=>81, "blue256"=>26),
    "wood brown3"=>array ("red256"=>143, "green256"=>107, "blue256"=>50),
    "wood brown2"=>array ("red256"=>179, "green256"=>146, "blue256"=>93),
    "wood brown1"=>array ("red256"=>222, "green256"=>188, "blue256"=>133),
    "brick red6"=>array ("red256"=>156, "green256"=>15, "blue256"=>15),
    "brick red5"=>array ("red256"=>191, "green256"=>3, "blue256"=>3),
    "brick red4"=>array ("red256"=>226, "green256"=>8, "blue256"=>0),
    "brick red3"=>array ("red256"=>232, "green256"=>87, "blue256"=>82),
    "brick red2"=>array ("red256"=>240, "green256"=>134, "blue256"=>130),
    "brick red1"=>array ("red256"=>249, "green256"=>204, "blue256"=>202),
    "raspberry pink6"=>array ("red256"=>156, "green256"=>15, "blue256"=>86),
    "raspberry pink5"=>array ("red256"=>191, "green256"=>3, "blue256"=>97),
    "raspberry pink4"=>array ("red256"=>226, "green256"=>0, "blue256"=>113),
    "raspberry pink3"=>array ("red256"=>232, "green256"=>82, "blue256"=>144),
    "raspberry pink2"=>array ("red256"=>240, "green256"=>130, "blue256"=>176),
    "raspberry pink1"=>array ("red256"=>249, "green256"=>202, "blue256"=>222),
    "burgundy purple6"=>array ("red256"=>106, "green256"=>0, "blue256"=>86),
    "burgundy purple5"=>array ("red256"=>133, "green256"=>2, "blue256"=>108),
    "burgundy purple4"=>array ("red256"=>160, "green256"=>39, "blue256"=>134),
    "burgundy purple3"=>array ("red256"=>177, "green256"=>79, "blue256"=>154),
    "burgundy purple2"=>array ("red256"=>193, "green256"=>115, "blue256"=>176),
    "burgundy purple1"=>array ("red256"=>232, "green256"=>183, "blue256"=>215),
    "grape violet6"=>array ("red256"=>29, "green256"=>10, "blue256"=>85),
    "grape violet5"=>array ("red256"=>52, "green256"=>23, "blue256"=>110),
    "grape violet4"=>array ("red256"=>70, "green256"=>40, "blue256"=>134),
    "grape violet3"=>array ("red256"=>100, "green256"=>74, "blue256"=>155),
    "grape violet2"=>array ("red256"=>142, "green256"=>121, "blue256"=>165),
    "grape violet1"=>array ("red256"=>195, "green256"=>180, "blue256"=>218),
    "skyblue6"=>array ("red256"=>0, "green256"=>49, "blue256"=>110),
    "skyblue5"=>array ("red256"=>0, "green256"=>67, "blue256"=>138),
    "skyblue4"=>array ("red256"=>0, "green256"=>87, "blue256"=>174),
    "skyblue3"=>array ("red256"=>44, "green256"=>114, "blue256"=>199),
    "skyblue2"=>array ("red256"=>97, "green256"=>147, "blue256"=>207),
    "skyblue1"=>array ("red256"=>164, "green256"=>192, "blue256"=>228),
    "sea blue6"=>array ("red256"=>0, "green256"=>72, "blue256"=>77),
    "sea blue5"=>array ("red256"=>0, "green256"=>96, "blue256"=>102),
    "sea blue4"=>array ("red256"=>0, "green256"=>120, "blue256"=>128),
    "sea blue3"=>array ("red256"=>0, "green256"=>167, "blue256"=>179),
    "sea blue2"=>array ("red256"=>0, "green256"=>196, "blue256"=>204),
    "sea blue1"=>array ("red256"=>168, "green256"=>221, "blue256"=>224),
    "emerald green6"=>array ("red256"=>0, "green256"=>88, "blue256"=>63),
    "emerald green5"=>array ("red256"=>0, "green256"=>115, "blue256"=>77),
    "emerald green4"=>array ("red256"=>0, "green256"=>153, "blue256"=>102),
    "emerald green3"=>array ("red256"=>0, "green256"=>179, "blue256"=>119),
    "emerald green2"=>array ("red256"=>0, "green256"=>204, "blue256"=>136),
    "emerald green1"=>array ("red256"=>153, "green256"=>220, "blue256"=>198),
    "forest green6"=>array ("red256"=>0, "green256"=>110, "blue256"=>41),
    "forest green5"=>array ("red256"=>0, "green256"=>137, "blue256"=>44),
    "forest green4"=>array ("red256"=>55, "green256"=>164, "blue256"=>44),
    "forest green3"=>array ("red256"=>119, "green256"=>183, "blue256"=>83),
    "forest green2"=>array ("red256"=>177, "green256"=>210, "blue256"=>143),
    "forest green1"=>array ("red256"=>216, "green256"=>232, "blue256"=>194),
    "sun yellow6"=>array ("red256"=>227, "green256"=>173, "blue256"=>0),
    "sun yellow5"=>array ("red256"=>243, "green256"=>195, "blue256"=>0),
    "sun yellow4"=>array ("red256"=>255, "green256"=>221, "blue256"=>0),
    "sun yellow3"=>array ("red256"=>255, "green256"=>235, "blue256"=>85),
    "sun yellow2"=>array ("red256"=>255, "green256"=>242, "blue256"=>153),
    "sun yellow1"=>array ("red256"=>255, "green256"=>246, "blue256"=>200),
    "hot orange6"=>array ("red256"=>172, "green256"=>67, "blue256"=>17),
    "hot orange5"=>array ("red256"=>207, "green256"=>73, "blue256"=>19),
    "hot orange4"=>array ("red256"=>235, "green256"=>115, "blue256"=>49),
    "hot orange3"=>array ("red256"=>242, "green256"=>155, "blue256"=>104),
    "hot orange2"=>array ("red256"=>242, "green256"=>187, "blue256"=>136),
    "hot orange1"=>array ("red256"=>255, "green256"=>217, "blue256"=>176),
    "aluminum gray6"=>array ("red256"=>46, "green256"=>52, "blue256"=>54),
    "aluminum gray5"=>array ("red256"=>85, "green256"=>87, "blue256"=>83),
    "aluminum gray4"=>array ("red256"=>136, "green256"=>138, "blue256"=>133),
    "aluminum gray3"=>array ("red256"=>186, "green256"=>189, "blue256"=>182),
    "aluminum gray2"=>array ("red256"=>211, "green256"=>215, "blue256"=>207),
    "aluminum gray1"=>array ("red256"=>238, "green256"=>238, "blue256"=>236),
    "brown orange6"=>array ("red256"=>77, "green256"=>38, "blue256"=>0),
    "brown orange5"=>array ("red256"=>128, "green256"=>63, "blue256"=>0),
    "brown orange4"=>array ("red256"=>191, "green256"=>94, "blue256"=>0),
    "brown orange3"=>array ("red256"=>255, "green256"=>126, "blue256"=>0),
    "brown orange2"=>array ("red256"=>255, "green256"=>191, "blue256"=>128),
    "brown orange1"=>array ("red256"=>255, "green256"=>223, "blue256"=>191),
    "red6"=>array ("red256"=>89, "green256"=>0, "blue256"=>0),
    "red5"=>array ("red256"=>140, "green256"=>0, "blue256"=>0),
    "red4"=>array ("red256"=>191, "green256"=>0, "blue256"=>0),
    "red3"=>array ("red256"=>255, "green256"=>0, "blue256"=>0),
    "red2"=>array ("red256"=>255, "green256"=>128, "blue256"=>128),
    "red1"=>array ("red256"=>255, "green256"=>191, "blue256"=>191),
    "pink6"=>array ("red256"=>115, "green256"=>0, "blue256"=>85),
    "pink5"=>array ("red256"=>163, "green256"=>0, "blue256"=>123),
    "pink4"=>array ("red256"=>204, "green256"=>0, "blue256"=>154),
    "pink3"=>array ("red256"=>255, "green256"=>0, "blue256"=>191),
    "pink2"=>array ("red256"=>255, "green256"=>128, "blue256"=>223),
    "pink1"=>array ("red256"=>255, "green256"=>191, "blue256"=>240),
    "purple6"=>array ("red256"=>44, "green256"=>0, "blue256"=>89),
    "purple5"=>array ("red256"=>64, "green256"=>0, "blue256"=>128),
    "purple4"=>array ("red256"=>90, "green256"=>0, "blue256"=>179),
    "purple3"=>array ("red256"=>128, "green256"=>0, "blue256"=>255),
    "purple2"=>array ("red256"=>192, "green256"=>128, "blue256"=>255),
    "purple1"=>array ("red256"=>223, "green256"=>191, "blue256"=>255),
    "blue6"=>array ("red256"=>0, "green256"=>0, "blue256"=>128),
    "blue5"=>array ("red256"=>0, "green256"=>0, "blue256"=>191),
    "blue4"=>array ("red256"=>0, "green256"=>0, "blue256"=>255),
    "blue3"=>array ("red256"=>0, "green256"=>102, "blue256"=>255),
    "blue2"=>array ("red256"=>128, "green256"=>179, "blue256"=>255),
    "blue1"=>array ("red256"=>191, "green256"=>217, "blue256"=>255),
    "green6"=>array ("red256"=>0, "green256"=>77, "blue256"=>0),
    "green5"=>array ("red256"=>0, "green256"=>140, "blue256"=>0),
    "green4"=>array ("red256"=>0, "green256"=>191, "blue256"=>0),
    "green3"=>array ("red256"=>0, "green256"=>255, "blue256"=>0),
    "green2"=>array ("red256"=>128, "green256"=>255, "blue256"=>128),
    "green1"=>array ("red256"=>191, "green256"=>255, "blue256"=>191),
    "lime6"=>array ("red256"=>99, "green256"=>128, "blue256"=>0),
    "lime5"=>array ("red256"=>139, "green256"=>179, "blue256"=>0),
    "lime4"=>array ("red256"=>191, "green256"=>245, "blue256"=>0),
    "lime3"=>array ("red256"=>255, "green256"=>255, "blue256"=>0),
    "lime2"=>array ("red256"=>240, "green256"=>255, "blue256"=>128),
    "lime1"=>array ("red256"=>248, "green256"=>255, "blue256"=>191),
    "yellow6"=>array ("red256"=>255, "green256"=>170, "blue256"=>0),
    "yellow5"=>array ("red256"=>255, "green256"=>191, "blue256"=>0),
    "yellow4"=>array ("red256"=>255, "green256"=>213, "blue256"=>0),
    "yellow3"=>array ("red256"=>255, "green256"=>255, "blue256"=>0),
    "yellow2"=>array ("red256"=>255, "green256"=>255, "blue256"=>153),
    "yellow1"=>array ("red256"=>255, "green256"=>255, "blue256"=>191),
    "gray6"=>array ("red256"=>50, "green256"=>50, "blue256"=>50),
    "gray5"=>array ("red256"=>85, "green256"=>85, "blue256"=>85),
    "gray4"=>array ("red256"=>136, "green256"=>136, "blue256"=>136),
    "gray3"=>array ("red256"=>187, "green256"=>187, "blue256"=>187),
    "gray2"=>array ("red256"=>221, "green256"=>221, "blue256"=>221),
    "gray1"=>array ("red256"=>238, "green256"=>238, "blue256"=>238)
);

$extracolors = array();
if (isset ($_GET ['color'])) {
  $extranames = split (",", $_GET ['color']);
  foreach ($extranames as $key=>$extraname) {
    $extraname = trim ($extraname);
    $hex = hexdec ($extraname);
    if (isset ($defaultcolors [$extraname]))
      $extracolors [$extraname] = $defaultcolors [$extraname];
    elseif (isset ($oxygencolors [$extraname]))
      $extracolors [$extraname] = $oxygencolors [$extraname];
    elseif (isset ($cigcolors [$extraname]))
      $extracolors [$extraname] = $cigcolors [$extraname];
    else {
      $red = $hex / 256 / 256 % 256;
      $green = $hex / 256 % 256;
      $blue = $hex % 256;
      $extraname = sprintf ("#%'02X%'02X%'02X", $red, $green, $blue);
      $extracolors [$extraname] = array ("red256"=>$red, "green256"=>$green, "blue256"=>$blue);
    }
  }
}

$luminosities = array();

foreach ($oxygencolors as $oxygenname=>$oxygencolor)
    computeColorFromRGB ($oxygencolors [$oxygenname]);
foreach ($cigcolors as $cigname=>$cigcolor)
    computeColorFromRGB ($cigcolors [$cigname]);
foreach ($defaultcolors as $defaultname=>$defaultcolor)
    computeColorFromRGB ($defaultcolors [$defaultname]);
foreach ($extracolors as $extraname=>$extracolor)
    computeColorFromRGB ($extracolors [$extraname]);

if ($_GET ["sortby"] == "name") {
  ksort ($defaultcolors);
  ksort ($oxygencolors);
  ksort ($cigcolors);
} elseif ($_GET ["sortby"] == "hex") {
  uasort ($defaultcolors, "compareHex");
  uasort ($oxygencolors, "compareHex");
  uasort ($cigcolors, "compareHex");
} elseif ($_GET ["sortby"] == "hue") {
  uasort ($defaultcolors, "compareHue");
  uasort ($oxygencolors, "compareHue");
  uasort ($cigcolors, "compareHue");
} elseif ($_GET ["sortby"] == "chroma") {
  uasort ($defaultcolors, "compareChroma");
  uasort ($oxygencolors, "compareChroma");
  uasort ($cigcolors, "compareChroma");
} elseif ($_GET ["sortby"] == "saturation") {
  uasort ($defaultcolors, "compareSaturation");
  uasort ($oxygencolors, "compareSaturation");
  uasort ($cigcolors, "compareSaturation");
} elseif ($_GET ["sortby"] == "brightness") {
  uasort ($defaultcolors, "compareBrightness");
  uasort ($oxygencolors, "compareBrightness");
  uasort ($cigcolors, "compareBrightness");
} elseif (count ($extracolors) > 0 && ! ($_GET ["sortby"] == "luminosity")) {
   uasort ($defaultcolors, "compareContrast");
   uasort ($oxygencolors, "compareContrast");
   uasort ($cigcolors, "compareContrast");
} else {
   uasort ($defaultcolors, "compareLuminosity");
   uasort ($oxygencolors, "compareLuminosity");
   uasort ($cigcolors, "compareLuminosity");
}
?>

<p>Ensuring proper contrast between forground and background colors is important for users
who are partially sighted, color-blind, have old eyes or who use a notebook screen or a projector
on a sunny day. Unfortunately it is difficult to objctively evaluate the contrast without the use of
mathematical calculations, especially since there are several different algorithms.
</p>

<p>This page is based on a
<a href="http://www.w3.org/TR/UNDERSTANDING-WCAG20/Overview.html#visual-audio-contrast-contrast">formula
from the current draft</a> of the Web Content Accessibility Guidelines 2.0 (WCAG2). The formula defines the
luminosity contrast as a number betwen 1 and 21. Foreground and background colors must have a luminosity
contrast of at least 5 for level 2 conformance, and 10 for level 3 conformance. The current version of the
Web Content Accessibility Guidelines (WCAG1) suggests <a href="http://www.w3.org/TR/2000/WD-AERT-20000426#color-contrast">a
different pair of algorithms</a> to check for brightness contrast and color contrast. The results of the
newer formula are <a href="http://juicystudio.com/article/luminositycontrastratioalgorithm.php">better</a>,
but since it has not been officially released yet, the older tests are shown as well in the detailed views.
</p>

<p>The values for luminosity, saturation and hue based on a <a href="hsl-adjusted.php">new sHSL color-space</a>,
which I have derived from the WCAG 2.0 and the sRGB definition. They are therefore different from
the values shown in other applications which ignore both the gamma correction and the different luminosity
of the various hues.
</p>

<h2>User-Chosen Colors</h2>
<form method="get" action="">
  <p><label for="color">Evaluate user-chosen colors:</label> <input type="text" name="color" id="color" />
  <input type="submit" value="submit" />
  <br/>(Please type in the hexadecimal values of the colors. Separate them with commas.)
  </p>
</form>

<?php
if (count ($extracolors) > 0) {
  printTable ($extracolors, "user-chosen");
}
?>
<h2>Suggested KDE4 Default Colors</h2>
<p>
The following table contains a suggestion for the default color scheme in KDE4.
It based on the KDE3 default colors and on the colors of the Oxygen icons for KDE4
(see below).
</p>
<?php
printTable ($defaultcolors, "suggested default");

?>
<h2>Oxygen Palette Colors</h2>
<p>
The following table contains the colors used by the Oxygen icon theme.
</p>
<?php
printTable ($oxygencolors, "Oxygen palette");

?>
<h2>KDE CIG Palette Colors</h2>
<p>
The following table contains the colors from the KDE CIG palette.
</p>
<?php
printTable ($cigcolors, "CIG palette");
?>

<?php
include "footer.inc";
?>
